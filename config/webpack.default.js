const path = require('path');
var WebpackObfuscator = require('webpack-obfuscator');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: './src/wrapper/default.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../dist/default'),
    },
    plugins: [
        // new CleanWebpackPlugin(),
        new WebpackObfuscator ({
            rotateUnicodeArray: true
        }, []),
    ],
};