import _ from 'lodash';
import './style.css';
import MaxApi from "@momo-platform/max-api";
import * as GameDefine from'./game/GameDefine';
//
if (process.env.NODE_ENV !== 'production') 
{
    console.warn('Looks like we are in development mode!');
}
else
{
    console.warn("Production mode, console cleared");
    console.log = function(){};
}

//
global.PIXI = require('pixi.js');
global.Utils = require('./core/Utils');
global.EventDefine = require('./events/EventDefine');
global.Override = require('./core/Override');
global.Input = require('./core/Input');
//

// const styles = {
//     canvasStyle : {
//         // position: 'absolute'
//     },
//     containerStyle : {
//         // textAlign: 'center'
//     },
//     gameDivStyle : {
//         // position: 'relative'
//         alignItems: 'center',
//         justifyContent: 'center',
//         /* align-self: center; */
//         height: '100vh',
//         display: 'flex',
//     }
// }

class Index
{
    constructor()
    {
        this.Resize;
        this.VisibleChange;
        this.AudioChange;
        this.singleCall;
    }

    CreateElement()
    {
        // const div = document.createElement('div');
        // div.id = 'canvasContainer';
        // for (let a in styles.gameDivStyle) div.style[a] = styles.gameDivStyle[a];
        // for (let a in styles.containerStyle) element.style[a] = styles.containerStyle[a];
        // //
        // document.body.appendChild(element);
    }

    MainGameClosure()
    {
        // (function() {
            // PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST;
            var APP = require('./app');
            var StateManager = require('./states/StateManager');
        
            var states = [
                new (require('./states/StatePreload')),
                new (require('./states/StateLoading')),
                new (require('./states/StateIngame')),
                new (require('./states/StateEnd'))
            ];
            // for (let a in styles.canvasStyle) APP.view.style[a] = styles.canvasStyle[a];
            document.getElementById('canvasContainer').appendChild(APP.view);
            //
            StateManager.AddState(states);
            //
            APP.Init();
            StateManager.Init();
            global.Resources = PIXI.Loader.shared.resources;
            //
            APP.renderer.plugins.interaction.moveWhenInside = true;
            this.Resize = function(size) { 
                APP.Resize(size);
            }

            this.VisibleChange = function(){

            }

            this.AudioChange = function() {

            }
        
            // global.resize({ width: innerWidth, height: innerHeight });
            
        // }.bind(this)());
    }

    OnReady()
    {
        if (!this.singleCall)
        {
            this.singleCall = true;
            // if (!GameDefine.isMobile())
            // {
                GameDefine.SetApiInfo();
                this.CreateElement();
                this.MainGameClosure();
                this.Resize({ width: innerWidth, height: innerHeight });
            // }
            // else
            // {
                MaxApi.init({
                    "appId": "vn.momo.web.momoacademy",
                    "name": "vn.momo.web.momoacademy",
                    "displayName": "MoMo Academy",
                    "client": {
                        "web": {
                            "hostId": "vn.momo.web.momoacademy",
                            "accessToken": "U2FsdGVkX194ZqKMPmzXS+UkePODhT0lCbCr6KttGeGWgmzKH6Y/QI4g2GeaQbKh+Qz3rW6MO5Z7GLxBKlYBPogGDZ2/QRQWueWL5AdGbOM="
                        }
                    },
                    "configuration_version": 1
                });

                MaxApi.getProfile((res) => {
                    GameDefine.userToken = res.userId;
                    GameDefine.SetApiInfo();
                    this.CreateElement();
                    this.MainGameClosure();
                    this.Resize({ width: innerWidth, height: innerHeight });
                });
            // }
        }
    }
}

export default Index;