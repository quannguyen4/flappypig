// const BaseView = require('./BaseView');
const APP = require('../../app');
const EventManager = require('../../events/EventManager');
const EventDefine = require('../../events/EventDefine');
const Input = require('../../core/Input');
const GameDefine = require('../../game/GameDefine');

class TutorialView extends PIXI.Container {
    constructor() {
        super();
        //
        this.mBackgroundLayer = new PIXI.Container();
        this.mForegroundLayer = new PIXI.Container();
        this.mTitle = null;
        // this.mHandWheel = null;
        // this.mWheelBackground = null;
        // this.mWheel = null;
        //
        this.addChild(this.mBackgroundLayer);
        this.addChild(this.mForegroundLayer);
        //
        this.isTouch = false;
        // this.mSpinSpeed = 0;
        this.isEnd = false;
        this.isLandscape = false;
        // EventManager.subscribe(EventDefine.ON_CONTAINER_RESIZE, this.Resize.bind(this));
        this.mTutorialText = null;
        this.mTutSprite = null;
        this.mTutHand = null;
    }

    Init() 
    {
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            this.mBg = new PIXI.Graphics();
            this.mBg.beginFill(0x000000, 0.7);
            this.mBg.drawRect(0, 0, APP.GetWidth(), APP.GetHeight());
            //
            this.mTutorialText = new PIXI.Text('NHẤN ĐỂ BẮT ĐẦU', {
                fontFamily: GameDefine.SUB_FONT_1.name,
                fill: '#dd6a08',
                fontSize: 60*GameDefine.PAD_SCALE,
                lineJoin: "bevel",
                stroke: "white",
                strokeThickness: 4,
                fontWeight: 'bold'
            });
            this.mTutorialText.anchor.set(0.5);
            this.mTutorialText.deltaAlpha = Math.PI/2;
            this.mTutSprite = new PIXI.Sprite(Resources.tutSprite.texture);
            this.mTutSprite.anchor.set(0.5);
            this.mTutHand = new PIXI.Sprite(Resources.tutHand.texture);
            this.mTutHand.anchor.set(0.5);
            this.mTutHand.baseScale = 0.75;
            this.mTutHand.deltaScale = 0;
            //
            // this.addChild(this.mBg);
            this.addChild(this.mTutSprite);
            this.addChild(this.mTutHand);
            this.addChild(this.mTutorialText);
            this.Eventhandler();
            //
            this.Scaling();
            this.Positioning();
        }
    }

    Eventhandler()
    {
        EventManager.subscribe(EventDefine.ON_TUTORIAL_PAD_COMPLETE, function(data){

        }.bind(this));
    }

    Scaling()
    {
        if (APP.GetWidth() > APP.GetHeight())
        {

        }
        else
        {
            this.mTutSprite.scale.set(0.75);
            this.mTutHand.scale.set(this.mTutHand.baseScale);
        }
    }

    Positioning() 
    {
        if (APP.GetWidth() > APP.GetHeight())
        {

        }
        else
        {
            this.mTutorialText.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight() + 300);
            this.mTutSprite.position.set(GameDefine.BIRD_SPAWN_X + 150, 0.5*APP.GetHeight() - 100);
            this.mTutHand.position.set(this.mTutSprite.position.x + 0.5*this.mTutSprite.width, this.mTutSprite.position.y + 0.5*this.mTutSprite.height );
        }
    }

    Resize(s)
    {
        if (s.data.width > s.data.height)
        {
            this.isLandscape = true;
        }
        else this.isLandscape = false;
    }

    Update(dt)
    {
        if (!this.visible)
            return;
        if (this.isEnd)
        {
            this.mTutorialText.alpha = Math.max(0, this.mTutorialText.alpha - 0.5*dt);
            this.alpha = Math.max(0, this.alpha -= 2*dt);
            if (this.alpha == 0 && this.visible)
                this.visible = false;
        }
        else
        {
            if (this.mTutHand.deltaScale >= 2*Math.PI)
                this.mTutHand.deltaScale = 0;
            this.mTutHand.scale.set(this.mTutHand.baseScale + 0.25*Math.sin(this.mTutHand.deltaScale += 5*dt));
            this.mTutorialText.alpha = 0.5*Math.sin(this.mTutorialText.deltaAlpha += 4*dt) + 0.5;
            if (this.mTutorialText.deltaAlpha > 2.5*Math.PI)
                this.mTutorialText.deltaAlpha = 0.5*Math.PI;
        }
    }

    TouchHandler(event) 
    {
        if (this.alpha == 0 || !this.visible)
            return;
        // switch (event.type) {
        //     case 'mousedown':
        //         console.log('touch');
        //         this.isTouch = true;
        //     break;

        //     case 'mousemove':
        //         // console.log('move');
        //         // console.log(event.data);
        //         // EventManager.publish(EventDefine.EVENT_PLAYER_ON_MOVE, event);
        //     break;
            
        //     case 'mouseup':
        //         this.isTouch = false;
        //         this.isEnd = true;
        //         EventManager.publish(EventDefine.SPIN_VIEW_END, {});
        //     break;
        // }
        if (Input.IsTouchDown(event))
        {
            this.isTouch = true;
        }
        if (Input.IsTouchUp(event))
        {
            this.isTouch = false;
            this.isEnd = true;
            // this.mRealSprite.Start();
            EventManager.publish(EventDefine.TUTORIAL_VIEW_END, {});
        }
    }
}

module.exports = TutorialView;