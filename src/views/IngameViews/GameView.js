const GameMgr = require('../../game/GameMgr');
const ScoreMgr = require('../../game/ScoreMgr');
const GameDefine = require('../../game/GameDefine');
const APP = require('../../app');
const EventManager = require('../../events/EventManager');
const EventDefine = require('../../events/EventDefine');
const Utils = require('../../core/Utils');
const ProgressBar = require('../../game/ProgressBar');

class GameView extends PIXI.Container
{
    constructor()
    {
        super();
        this.mGameMgr;
        this.mIsFirstInit = false;
        this.mScoreText = {};
        this.mPrizeBar = null;
    }

    Init()
    {
        if (!this.mIsFirstInit)
        {
            const style = new PIXI.TextStyle({
                fontFamily: GameDefine.MAIN_FONT.name,
                fill: '#dd6a08',
                fontSize: 75*GameDefine.PAD_SCALE,
                lineJoin: "bevel",
                stroke: "white",
                strokeThickness: 10,
                fontWeight: 'bold'
            });
            //
            this.mIsFirstInit = true;
            this.mGameMgr = new GameMgr();
            this.mScoreText = new PIXI.Text('0',  style);
            this.mScoreText.anchor.set(0, 0.5);
            this.mScoreText.scaleDelta = 3*Math.PI;
            //
            this.mPrizeBar = new ProgressBar();
            //
            this.mGameMgr.Init();
            this.mPrizeBar.Init();
            //
            this.addChild(this.mGameMgr);
            this.addChild(this.mScoreText);
            this.addChild(this.mPrizeBar);
            this.Positioning();
            this.EventHandler();
        }
    }

    EventHandler()
    {
        EventManager.subscribe(EventDefine.ON_SCORE_SUCCESS, function(){
            this.mPrizeBar.SetProgress(ScoreMgr.GetScore()/GameDefine.GAME_MAX_SCORE);
        }.bind(this));

        EventManager.subscribe(EventDefine.TUTORIAL_VIEW_END, function(){
            this.mPrizeBar.Start();
        }.bind(this));

        EventManager.subscribe(EventDefine.ON_WAIT_RESTART, function(){
            this.mPrizeBar.SetProgress(0);
        }.bind(this));

        EventManager.subscribe(EventDefine.REWARD_CONFIG_LOAD_COMPLETE, function(){
            let imgs = [Resources.rewardBag.texture, Resources.rewardBag.texture, Resources.rewardCase.texture];
            let data = GameDefine.REWARD_CONFIG.data.map((d, i) => {
                return {
                    image: imgs[i],
                    percent: d.point/GameDefine.GAME_MAX_SCORE,
                    text: d.point + '',
                }
            });
            this.mPrizeBar.SetMarkerData(data);
        }.bind(this));
    }

    Positioning()
    {
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.mScoreText.position.set(0.25*APP.GetWidth(), 0.5*APP.GetHeight() - 50);
        }
        else
        {
            this.mPrizeBar.position.set(200, 200);
            this.mScoreText.position.set(this.mPrizeBar.position.x + this.mPrizeBar.width + 75, this.mPrizeBar.position.y + 0.25*this.mScoreText.height);
        }
    }

    Update(dt)
    {
        if (this.mGameMgr)
            this.mGameMgr.Update(dt);
        this.mScoreText.text = ScoreMgr.GetScore() + '';
        this.mPrizeBar.Update(dt);
    }
}
module.exports = GameView;