const BaseView = require('./BaseView');
const APP = require('../app');
const GameView = require('./IngameViews/GameView');
const EventManager = require('../events/EventManager');
const EventDefine = require('../events/EventDefine');
const GameDefine = require('../game/GameDefine');
const MainMusic = require('../game/MainMusic');
const Input = require('../core/Input');
const Timer = require('../core/Timer');
const TutorialView = require('./IngameViews/TutorialView');
const Utils = require('../core/Utils');
const Sample = require('../game/Sample');
const firstRank = require('../assets/images/first_rank.png');
const secondRank = require('../assets/images/second_rank.png');
const thirdRank = require('../assets/images/third_rank.png');
const Button = require('../game/Elements/Button');
const MaxApi = require('@momo-platform/max-api').default;

class StateIngameView extends BaseView {
    constructor() {
        super();
        //
        this.mBackgroundLayer = new PIXI.Container();
        this.mForegroundLayer = new PIXI.Container();
        //
        this.addChild(this.mBackgroundLayer);
        this.addChild(this.mForegroundLayer);
        //
        this.mGameView = new GameView();
        this.mTutorialView = new TutorialView();
        //
        this.mBg = null;
        // this.mMusic = new MainMusic({
        //     pauseDuration: GameDefine.GAME_TIME_PAUSE_BEFORE_PLAY,
        //     src: [Utils.GetAssetUrl("sounds/DanceMonkeyCover_TonesAndI.mp3")]
        //     // src: [Utils.GetAssetUrl("sounds/TheBlackEyedPeas_ads.mp3")]
        // });
        this.mMusic = null;
        this.isPause = false;
        this.mQuestionBtn = null;
        this.mScoreboardBtn = null;
        this.mPauseResumeBtn = null;
        this.mBackBtn = null;
        // this.isFirstPlay = true;
    }

    Init() 
    {
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            this.mMusic = MainMusic;
            this.mBg = new PIXI.Sprite(Resources.ingameBg.texture);
            this.mBg.deltaAlpha = 0;
            this.mBg.anchor.set(0.5);
            // if (APP.GetWidth() > APP.GetHeight())
                this.mBg.scale.set(APP.GetHeight()/this.mBg.height);
            // else
            //     this.mBg.scale.set(APP.GetWidth()/this.mBg.width ,APP.GetHeight()/this.mBg.height);
            //
            this.mGameView.Init();
            this.mTutorialView.Init();
            // console.log(this.mMusic.duration());
            //
            // this.mPlayTimer.Stop();
            this.mQuestionBtn = new Button(Resources.questionBtn.texture);
            // this.mQuestionBtn.anchor.set(0.5);
            this.mScoreboardBtn = new Button(Resources.scoreboardBtn.texture);
            this.mBackBtn = new Button(Resources.backBtn.texture);
            // this.mScoreboardBtn.anchor.set(0.5);
            this.mPauseResumeBtn = new Button(Resources.resumeBtn.texture);
            // this.mPauseResumeBtn.anchor.set(0.5);
            this.mPauseResumeBtn.visible = false;
            this.mPauseResumeBtn.alpha = 0;
            //
            this.addChild(this.mBg);
            this.addChild(this.mGameView);
            this.addChild(this.mTutorialView);
            this.addChild(this.mQuestionBtn);
            this.addChild(this.mScoreboardBtn);
            this.addChild(this.mPauseResumeBtn);
            this.addChild(this.mBackBtn);
            //
            this.Scaling();
            this.Positioning();
            this.SetTouchable(true);
            this.EventHandler();
            this.LoadLeaderboard(true, true);
            this.LoadReward();
            // this.mEndView.Hide();
            // MaxApi.getItem('firstPlay'
            MaxApi.getItem('firstPlay', function(res){
                if (!res)
                {
                    MaxApi.setItem('firstPlay', true);
                    showDialog('contentFirstPlay');
                }
            }.bind(this));
        }
    }

    Scaling()
    {
        if (APP.GetWidth() > APP.GetHeight())
        {
            
        }
        else
        {
            this.mBackBtn.scale.set(1.5);
            this.mQuestionBtn.scale.set(1.5);
            this.mScoreboardBtn.scale.set(1.5);
            this.mPauseResumeBtn.scale.set(1.5);
        }
    }

    Positioning() 
    {
        this.mBg.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight());
        if (APP.GetWidth() > APP.GetHeight())
        {

        }
        else
        {
            this.mBackBtn.position.set(100, 200 + 0.5*this.mBackBtn.height);
            this.mQuestionBtn.position.set(APP.GetWidth() - 0.5*this.mQuestionBtn.width - 50, 400);
            this.mScoreboardBtn.position.set(
                this.mQuestionBtn.position.x, 
                this.mQuestionBtn.position.y + 0.5* this.mQuestionBtn.height + 0.5* this.mScoreboardBtn.height + 20
            );
            this.mPauseResumeBtn.position.set(APP.GetWidth() - 0.5* this.mPauseResumeBtn.width - 50, 400);
        }
    }

    Update(dt)
    {
        this.mGameView.Update(dt);
        this.mTutorialView.Update(dt);
        if (this.mTutorialView.isEnd)
        {
            this.mQuestionBtn.alpha = Math.max(0, this.mQuestionBtn.alpha - 2*dt);
            this.mScoreboardBtn.alpha = Math.max(0, this.mScoreboardBtn.alpha - 2*dt);
            this.mPauseResumeBtn.alpha = Math.min(1, this.mPauseResumeBtn.alpha + 2*dt);
            if (this.mQuestionBtn.alpha == 0) this.mQuestionBtn.visible = false;
            if (this.mScoreboardBtn.alpha == 0) this.mScoreboardBtn.visible = false;
        }
    }

    EventHandler()
    {
        window.addEventListener('firstPlayClick', function(){
            this.isFirstPlay = false;
        }.bind(this));
        window.addEventListener('showScoreboard', function(){
            hideDialog();
            setTimeout(function(){
                showDialog('scoreBoard');
                this.LoadLeaderboard();
            }.bind(this), 500);
        }.bind(this));
        EventManager.subscribe(EventDefine.TUTORIAL_VIEW_END, function(){
            this.mPauseResumeBtn.visible = true;
        }.bind(this));
        document.getElementById('btnMonthLeader').onclick = function(){
            GameDefine.IS_MONTH = true;
            this.LoadLeaderboard(!document.getElementById('onlyFriend').checked, GameDefine.IS_MONTH);
        }.bind(this);
        document.getElementById('btnAllTimeLeader').onclick = function(){
            GameDefine.IS_MONTH = false;
            this.LoadLeaderboard(!document.getElementById('onlyFriend').checked, GameDefine.IS_MONTH);
        }.bind(this);
        document.getElementById('onlyFriend').onchange = function(){
            this.LoadLeaderboard(!document.getElementById('onlyFriend').checked, GameDefine.IS_MONTH);
        }.bind(this);
    }

    async LoadReward()
    {
        if (GameDefine.REWARD_CONFIG)
            return;
        let returnValue = null;
        await fetch(GameDefine.REWARD_CONFIG_URL, 
        {
            method: 'GET',
            credentials: 'include',
            withCredentials: true,
            headers: new Headers({
                ...GameDefine.HEADER_MAIN_CONFIG
            }),
        }).then(response => response.json())
        .then(function(res){
            returnValue = res;
        })
        .catch(function(error) {
            // let s = Sample.reward_sample;
            // returnValue = s;
            return error;
        });
        
        GameDefine.REWARD_CONFIG = returnValue;
        this.ShowReward(GameDefine.REWARD_CONFIG.data);
        EventManager.publish(EventDefine.REWARD_CONFIG_LOAD_COMPLETE, {});
    }

    async LoadLeaderboard(isGlobal = true, inMonth = true)
    {
        let d = GameDefine.GetLeaderboardData(isGlobal, inMonth);
        if (Object.keys(d).length > 0)
        {
            GameDefine.LEADERBOARD_DATA = d;
            this.ShowLeaderboard(d.items);
            return;
        }
        let returnValue = null;
        await fetch(`${GameDefine.LEADERBOARD_URL}${isGlobal ? 'global' : 'friends'}${GameDefine.LEADERBOARD_PREFIX}&${inMonth ? 'medal_types=flappypig_point_month' : 'medal_types=flappypig_point'}`, 
        {
            method: 'GET',
            credentials: 'include',
            withCredentials: true,
            headers: new Headers({
                ...GameDefine.HEADER_MAIN_CONFIG
            }),
        }).then(response => response.json())
        .then(function(res) {
            returnValue = res;
        })
        .catch(function(error) {
            return error;
        });
        
        d = returnValue;
        GameDefine.LEADERBOARD_DATA = d;
        for (let a in d)
            GameDefine.GetLeaderboardData(isGlobal, inMonth)[a] = d[a];
        // GameDefine.AddDebugLog(JSON.stringify(GameDefine.LEADERBOARD_DATA));
        this.ShowLeaderboard(d.items);
    }

    ShowLeaderboard(data)
    {
        let parent = document.getElementById('scoreItemContainer');
        let curProfile = document.getElementById('currentProfile');
        curProfile.getElementsByClassName('rankImg')[0].innerHTML = '';
        curProfile.getElementsByClassName('rankImg')[0].style.background = '';
        curProfile.getElementsByClassName('rankImg')[0].classList.remove('rankImgText');
        parent.innerHTML = '';
        for (let i =0; i< data.length; i++)
        {
            let el = document.getElementById('leaderboardItem').cloneNode(true);
            el.id = null;
            el.style.display = null;
            el.getElementsByClassName('profileName')[0].innerHTML = data[i].name;
            el.getElementsByClassName('profilePoint')[0].innerHTML = data[i].point;
            parent.appendChild(el);
        }
        curProfile.getElementsByClassName('profileName')[0].innerHTML = GameDefine.LEADERBOARD_DATA.meta.name;
        curProfile.getElementsByClassName('profilePoint')[0].innerHTML = GameDefine.LEADERBOARD_DATA.meta.point;
        if (GameDefine.LEADERBOARD_DATA.meta.rank <= 3)
        {
            switch (GameDefine.LEADERBOARD_DATA.meta.rank)
            {
                case 1:
                    curProfile.getElementsByClassName('rankImg')[0].style.background= `transparent url(${firstRank.default}) no-repeat center`;
                break;

                case 2:
                    curProfile.getElementsByClassName('rankImg')[0].style.background= `transparent url(${secondRank.default}) no-repeat center`;
                break;

                case 3:
                    curProfile.getElementsByClassName('rankImg')[0].style.background= `transparent url(${thirdRank.default}) no-repeat center`;
                break;
            }
        }
        else
        {
            curProfile.getElementsByClassName('rankImg')[0].classList.add('rankImgText');
            curProfile.getElementsByClassName('rankImg')[0].innerHTML = GameDefine.LEADERBOARD_DATA.meta.rank;
        }
        renderRankDOM();
    }

    ShowReward(data)
    {
        let parent = document.getElementById('prizeContainer');
        parent.innerHTML = '';
        for (let i =0; i< data.length; i++)
        {
            let el = document.getElementById('rewardItem').cloneNode(true);
            el.id = null;
            el.style.display = null;
            el.getElementsByClassName('rewardQuantity')[0].innerHTML = data[i].quantity + ' ' + data[i].unit;
            el.getElementsByClassName('rewardCaption')[0].innerHTML = data[i].gift;
            el.getElementsByClassName('rewardPoint')[0].innerHTML = `Cần đạt <br/>${data[i].point} điểm`;
            parent.appendChild(el);
        }
    }

    Pause()
    {
        // this.mMusic.pause();
        this.isPause = true;
        // this.mPauseResumeBtn.texture = Resources.pauseBtn.texture;
        this.mPauseResumeBtn.setIcon(Resources.pauseBtn.texture);
    }

    Resume()
    {
        // this.mMusic.play();
        this.isPause = false;
        // this.mPauseResumeBtn.texture = Resources.resumeBtn.texture;
        this.mPauseResumeBtn.setIcon(Resources.resumeBtn.texture);
    }

    TouchHandler(event) 
    {
        if (Input.IsTouchDown(event))
        {
            let touchPoint = {x: event.data.global.x, y: event.data.global.y}
            if (
                (Utils.PointInRect(touchPoint, this.mQuestionBtn.getBounds()) && this.mQuestionBtn.alpha == 1)
                || (Utils.PointInRect(touchPoint, this.mScoreboardBtn.getBounds()) && this.mScoreboardBtn.alpha == 1)
                || (Utils.PointInRect(touchPoint, this.mPauseResumeBtn.getBounds()) && this.mPauseResumeBtn.alpha == 1)
            )
                return;
            if (
                Utils.PointInRect(touchPoint, this.mBackBtn.getBounds())
            )
            {
                MaxApi.goBack();
            }
            if (!this.isPause)
            {
                EventManager.publish(EventDefine.ON_TOUCH, {event});
            }
        }
        if (Input.IsTouchUp(event))
        {
            let touchPoint = {x: event.data.global.x, y: event.data.global.y}
            if (Utils.PointInRect(touchPoint, this.mQuestionBtn.getBounds()) && this.mQuestionBtn.alpha == 1)
            {
                this.LoadReward();
                showDialog('contentFirstPlay');
                return;
            }
            if (Utils.PointInRect(touchPoint, this.mScoreboardBtn.getBounds()) && this.mScoreboardBtn.alpha == 1)
            {
                showDialog('scoreBoard');
                this.LoadLeaderboard();
                return;
            }
            if (Utils.PointInRect(touchPoint, this.mPauseResumeBtn.getBounds()) && this.mPauseResumeBtn.alpha == 1)
            {
                let e = !this.isPause ? new CustomEvent('gamePause', {}) : new CustomEvent('gameResume', {});
                window.dispatchEvent(e);
            }
        }
        this.mTutorialView.TouchHandler(event);
    }
}

module.exports = StateIngameView;