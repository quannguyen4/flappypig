const BaseView = require('./BaseView');
const APP = require('../app');
const Timer = require('../core/Timer');

class StateEndView extends BaseView {
    constructor() {
        super();
        //
        this.mBackgroundLayer = new PIXI.Container();
        this.mForegroundLayer = new PIXI.Container();
        //
        this.addChild(this.mBackgroundLayer);
        this.addChild(this.mForegroundLayer);
        //
        this.mBg = null;
        this.mIsFirstInit = false;
        this.mCTA = null;
        this.mTitle = null;
        this.mSongList = null;
        this.mLogo = null;
        this.mReplay = null;
        this.mAmaLogo = null;
        this.mRotateTimer = new Timer();
    }

    Init() 
    {
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            this.mBg = new PIXI.Sprite(Resources.outtroBg.texture);
            this.mBg.anchor.set(0.5);
            this.mLogo = new PIXI.Sprite(Resources.logo.texture);
            this.mLogo.anchor.set(0.5);
            this.mTitle = new PIXI.Sprite(Resources.chooseSongLandscape.texture);
            this.mTitle.anchor.set(0.5);
            this.mReplay = new PIXI.Sprite(Resources.replay.texture);
            this.mReplay.anchor.set(0.5);
            this.mCTA = new PIXI.Sprite(Resources.cta.texture);
            this.mCTA.anchor.set(0.5);
            this.mAmaLogo = new PIXI.Sprite(Resources.amaLogo.texture);
            this.mAmaLogo.anchor.set(0.5);
            this.mSongList = new PIXI.Sprite(Resources.songList.texture);
            this.mSongList.anchor.set(0.5);
            //
            this.mBg.alpha = 0.5;
            this.mCTA.baseScale = 1;
            this.mCTA.deltaScale = 1.25*Math.PI;
            this.mCTA.animScale = 0;
            this.mReplay.rotateTime = 0.5;
            this.mReplay.rotateSpeed = 1.41*(2*Math.PI/this.mReplay.rotateTime);
            this.mReplay.rotateAcc = (2*Math.PI/this.mReplay.rotateTime)/this.mReplay.rotateTime;
            this.mRotateTimer.SetDuration(3.5);
            this.mSongList.deltaAlpha = 0;
            //
            this.Scaling();
            this.Positioning();
            this.SetTouchable(true);
            this.hitArea = new PIXI.Rectangle(0, 0, APP.GetWidth(), APP.GetHeight());
            this.mCTA.SetTouchable(true);
            this.mSongList.SetTouchable(true);
            this.mReplay.SetTouchable(true);
            // this.EventHandler();
            this.addChild(this.mBg);
            this.addChild(this.mLogo);
            this.addChild(this.mTitle);
            this.addChild(this.mReplay);
            this.addChild(this.mCTA);
            this.addChild(this.mAmaLogo);
            this.addChild(this.mSongList);
        }
    }

    Scaling()
    {
        this.mBg.scale.set(APP.GetWidth()/this.mBg.width, APP.GetHeight()/this.mBg.height);
        if (APP.GetWidth() < APP.GetHeight())
        {
            let r = Math.min(1, (APP.GetHeight()/APP.GetWidth())/(1366/768));
            this.mTitle.scale.set(r*1);
            // this.mCTA.scale.set(1.35);
            this.mSongList.scale.set(r * 0.8);
            this.mLogo.scale.set(r*1.25);
            this.mCTA.scale.set(r * 1);
            this.mReplay.scale.set(r * 1);
            this.mAmaLogo.scale.set(r * 1);
        }
        else
        {
            let r = Math.min(0.9, APP.GetWidth()/1366);
            this.mLogo.scale.set(r * 0.75);
            this.mTitle.scale.set(r * 0.6);
            this.mCTA.scale.set(r * this.mCTA.baseScale);
            this.mReplay.scale.set(r * 0.75);
            this.mSongList.scale.set(r * 0.75);
            this.mAmaLogo.scale.set(r * 0.75);
        }
    }

    Positioning() 
    {
        this.mBg.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight());
        if (APP.GetWidth() < APP.GetHeight())
        {
            let r = APP.GetHeight()/1366;
            this.mTitle.texture = Resources.chooseSongPortrait.texture;
            this.mSongList.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight());
            this.mLogo.position.set(0.5*APP.GetWidth(), r*150);
            this.mTitle.position.set(this.mLogo.position.x, this.mLogo.position.y + 0.5*this.mLogo.height + 50);
            this.mReplay.position.set(0.5*APP.GetWidth(), APP.GetHeight() - 375*(APP.GetHeight()/APP.GetWidth())/(1366/768));
            this.mCTA.position.set(this.mReplay.position.x, this.mReplay.position.y + 0.5*this.mReplay.height + 50);
            this.mAmaLogo.position.set(this.mCTA.position.x, this.mCTA.position.y + 0.5*this.mCTA.height + 50);
        }
        else
        {
            this.mTitle.texture = Resources.chooseSongLandscape.texture;
            this.mSongList.position.set(0.25*APP.GetWidth() + 50, 0.5*APP.GetHeight());
            this.mLogo.position.set(0.75*APP.GetWidth() + 50, 100);
            this.mTitle.position.set(this.mLogo.position.x, this.mLogo.position.y + 0.5*this.mLogo.height + 40);
            this.mReplay.position.set(this.mLogo.position.x, APP.GetHeight() - 300);
            this.mCTA.position.set(this.mLogo.position.x, this.mReplay.position.y + 0.5*this.mReplay.height + 60);
            this.mAmaLogo.position.set(this.mLogo.position.x, APP.GetHeight() - 50);
        }
    }

    Update(dt)
    {
        if (this.mCTA)
        {
            this.mCTA.animScale = 0.15*Math.cos(this.mCTA.deltaScale += 2*dt);
            this.mCTA.scale.set(this.mCTA.baseScale + this.mCTA.animScale);
        }
        if (this.mReplay)
        {
            this.mRotateTimer.Update(dt);
            if (this.mRotateTimer.IsDone())
            {
                this.mReplay.rotateSpeed = 1.41*(2*Math.PI/this.mReplay.rotateTime);
                this.mRotateTimer.Reset();
            }
            this.mReplay.rotation = Math.min(2*Math.PI, this.mReplay.rotation - this.mReplay.rotateSpeed*dt);
            this.mReplay.rotateSpeed = Math.max(0, this.mReplay.rotateSpeed - this.mReplay.rotateAcc*dt);
            if (this.mReplay.rotateSpeed == 0)
            {
                this.mReplay.rotation = 0;
            }
        }
        if (this.mSongList)
        {
            this.mSongList.alpha = 0.2*Math.cos(this.mSongList.deltaAlpha += 4*dt) + 0.8;
        }
    }

    EventHandler()
    {

    }

    TouchHandler(event) 
    {
        if (Input.IsTouchDown(event))
        {
            // console.log(event.currentTarget, this.mCardView);
            if (event.target == this.mCTA || event.target == this.mReplay || event.target == this.mSongList)
            {
                let event = new CustomEvent('onCTAClick', {
                    detail: {url: 'https://apps.apple.com/us/app/magic-pad-music-beat-maker/id1465864127'}
                });
                window.dispatchEvent(event);
            }
        }
        if (Input.IsTouchUp(event))
        {

        }
    }
}

module.exports = StateEndView;