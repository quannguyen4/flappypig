const Utils = require("../../core/Utils");
const GameDefine = require("../GameDefine");

class Cloud extends PIXI.Container
{
    constructor()
    {
        super();
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++,
            INACTIVE: n++,
        };
        this.mState = null;
        this.mObject = null;
        this.mIsFirstInit = false;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch(this.mState)
        {
            case this.STATE.RUN:
                this._UpdatePosition(dt);
                this._UpdateInactive();
            break;
        }
    }

    IsInactive()
    {
        return this.mState == this.STATE.INACTIVE;
    }

    _UpdatePosition(dt)
    {
        this.position.x = this.position.x - 0.25*GameDefine.BIRD_MOVE_SPEED*this.mObject.alpha * dt;
    }

    _UpdateInactive()
    {
        if (this.getGlobalPosition().x < 0 - 0.5*this.width)
        {
            this._SetState(this.STATE.INACTIVE);
        }
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                    this.mObject = new PIXI.Sprite(Resources.ingameCloud.texture);
                    this.mObject.anchor.set(0.5);
                    //
                    this.addChild(this.mObject);
                }
            break;

            case this.STATE.RUN:
                let arr = [-2, 2];
                let dir = arr[Utils.RandInt(0, arr.length - 1)];
                this.mObject.alpha = Utils.Rand(0.5, 1);
                this.mObject.scale.set(dir*this.mObject.alpha, 2 * this.mObject.alpha);
            break;
        }
    }
}

module.exports = Cloud;