const GameDefine = require("./GameDefine");

class ProgressBar extends PIXI.Container
{
    constructor()
    {
        super();
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        this.mIsFirstInit = false;
        this.mBackground = null;
        this.mForegound = null;
        this.mMask = null;
        this.mProgress = 0;
        this.mMarkers = new PIXI.Container();
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    SetProgress(p)
    {
        this.mProgress = Math.min(p, 1);
        this.mMarkers.children.forEach(c => {
            if (this.mProgress >= c.percent)
            {
                if (!c.isComplete)
                {
                    c.isComplete = true;
                    c.onComplete();
                }
            }
        });
    }

    SetMarkerData(m)
    {
        this.mMarkers.removeChildren();
        for (let i = 0; i< m.length; i++)
        {
            let marker = this.CreateMarker(m[i]);
            marker.percent = m[i].percent;
            marker.position.set(m[i].percent*this.mBackground.width + this.mBackground.position.x - 8, this.mBackground.position.y + this.mBackground.height);
            this.mMarkers.addChild(marker);
        }
    }

    CreateMarker(marker)
    {
        const style = new PIXI.TextStyle({
            fontFamily: GameDefine.MAIN_FONT.name,
            fill: '#e5a227',
            fontSize: 60*GameDefine.PAD_SCALE,
            lineJoin: "bevel",
            stroke: "#3c661d",
            strokeThickness: 10,
            fontWeight: 'bold'
        });
        //
        let s = new PIXI.Container();
        let m = new PIXI.Sprite(Resources.triangleMarker.texture);
        let i = new PIXI.Sprite(marker.image);
        let t = new PIXI.Text(marker.text, style);
        let g = new PIXI.Graphics()
        .beginFill(0xffffff, 0.9)
        .drawRoundedRect(0, 0, t.width + 50, t.height, 40)
        .endFill();
        t.anchor.set(0.5);
        g.position.set(-0.5*g.width, m.height);
        t.position.set(g.position.x + 0.5*g.width, g.position.y + 0.5*g.height);
        m.anchor.set(0.5, 0);
        i.anchor.set(0.5, 1);
        i.scale.set(0.75);
        s.addChild(g, m, i, t);
        s.isComplete = false;
        s.onComplete = function(){
            let c = new PIXI.Sprite(Resources.checkMark.texture);
            c.anchor.set(0.5);
            c.scale.set(1.25);
            c.position.set(g.position.x + 0.5*g.width, g.position.y + 0.5*g.height);
            s.removeChild(t);
            s.addChild(c);
        };
        //
        return s;
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                this.mForegound.position.x = this.mForegound.position.x + 4*(this.mProgress*this.mForegound.width + 8 - this.mForegound.width - this.mForegound.position.x) * dt;
            break;
        }
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                    this.mBackground = new PIXI.Sprite(Resources.barBackground.texture);
                    this.mForegound = new PIXI.Sprite(Resources.barForeground.texture);
                    this.mForegound.position.set(8, 8);
                    //
                    this.mMask = new PIXI.Graphics()
                    .beginFill(0x000000, 0.7)
                    .drawRoundedRect(this.mForegound.position.x, this.mForegound.position.y, this.mForegound.width, this.mForegound.height, 27)
                    .endFill();
                    //
                    this.scale.set(0.75);
                    this.mForegound.mask = this.mMask;
                    //
                    this.addChild(this.mBackground);
                    this.addChild(this.mForegound);
                    this.addChild(this.mMask);
                    this.addChild(this.mMarkers);
                    //
                    this.mForegound.position.x -= this.mForegound.width;
                }
            break;

            case this.STATE.RUN:
            break;
        }
    }
}

module.exports = ProgressBar;