const APP = require('../../app');
const GameDefine = require('../GameDefine');

class PadClick extends PIXI.Container
{
    constructor()
    {
        super();
        this.mState = null;
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++,
            INACTIVE: n++,
        };
        this.mObject = null;
        this.mIsFirstInit = false;
        this.mResponScale = 0.85*APP.GetHeight()/768;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                this.mObject.update(dt);
            break;

            case this.STATE.INACTIVE:
            break;
        }
    }

    IsInactive()
    {
        return this.mState == this.STATE.INACTIVE;
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                    this.mObject = new PIXI.AnimatedSprite(Resources.explodeAnim.frames);
                    this.mObject.anchor.set(0.5);
                    //
                    this.mObject.scale.set(4*GameDefine.PAD_SCALE);
                    this.mObject.animationSpeed = 0.75;
                    this.mObject.loop = false;
                    //
                    this.addChild(this.mObject);
                }
            break;

            case this.STATE.RUN:
                this.mObject.gotoAndPlay(0);
            break;

            case this.STATE.INACTIVE:
            break;
        }
    }
}

module.exports = PadClick;