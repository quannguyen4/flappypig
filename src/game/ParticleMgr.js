const ObjectMgr = require('./ObjectMgr');
const GameDefine = require('./GameDefine');
const Cloud = require('./Particles/Cloud');
const Timer = require('../core/Timer');
const APP = require('../app');
const Utils = require('../core/Utils');

class ParticleMgr
{
    constructor()
    {
        // super();
        let n = 0;
        this.mState = null;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        this.mClouds = new ObjectMgr(Cloud);
        this.mCloudSpawnTimer = new Timer();
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch(this.mState)
        {
            case this.STATE.RUN:
                this.mClouds.Update(dt);
                this.mCloudSpawnTimer.Update(dt);
                if (this.mCloudSpawnTimer.IsDone())
                {
                    this.mCloudSpawnTimer.Reset();
                    this.SpawnParticle(GameDefine.PARTICLE_CLOUD, {x: APP.GetWidth() + 200, y: Utils.Rand(200, APP.GetHeight() - 200)});
                }
            break;
        }
    }

    GetParticleContainer(c)
    {
        switch (c)
        {
            case GameDefine.PARTICLE_CLOUD:
                return this.mClouds;
        }
    }

    SpawnParticle(particle, position)
    {
        let p;
        switch (particle)
        {
            case GameDefine.PARTICLE_CLOUD:
                p = this.mClouds._GetInstance();
            break;
        }
        p.Init();
        p.position.set(position.x, position.y);
        p.Start();
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                // this.addChild(this.mPerfectParticles);
                this.mCloudSpawnTimer.SetDuration(4);
            break;

            case this.STATE.RUN:
                // console.log('start');
            break;
        }
    }
}

const p = new ParticleMgr();
p.Init();

module.exports = p;