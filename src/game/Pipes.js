const Input = require("../core/Input");
const EventDefine = require("../events/EventDefine");
const EventManager = require("../events/EventManager");
const BaseObject = require("./BaseObject");
const GameDefine = require('./GameDefine');

class Pipes extends BaseObject
{
    constructor()
    {
        super();
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++,
            INACTIVE: n++,
        };
        this.mState = null;
        this.mIsFirstInit = false;
        this.mTopPipe = null;
        this.mBotPipe = null;
        this.isPassed = false;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                this._UpdatePosition(dt);
                this._UpdatePassed(dt);
                this._UpdateInactive();
            break;
        }
    }

    IsInactive()
    {
        return this.mState == this.STATE.INACTIVE;
    }

    SetPipeSpace(s)
    {
        this.mTopPipe.position.y = 0;
        this.mBotPipe.position.y = 0;
        this.mTopPipe.position.y -= s/2;
        this.mBotPipe.position.y += s/2;
    }

    _UpdatePassed(dt)
    {
        if (this.getGlobalPosition().x < GameDefine.BIRD_SPAWN_X && !this.isPassed)
        {
            this.isPassed = true;
            EventManager.publish(EventDefine.ON_SCORE_SUCCESS, {data: [this.mTopPipe.getGlobalPosition().y, this.mBotPipe.getGlobalPosition().y]});
        }
    }

    _UpdatePosition(dt)
    {
        this.position.x = this.position.x - GameDefine.BIRD_MOVE_SPEED*dt;
    }

    _UpdateInactive()
    {
        if (this.getGlobalPosition().x < 0 - 0.5*this.width)
        {
            this._SetState(this.STATE.INACTIVE);
        }
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                    this.mTopPipe = new PIXI.Sprite(Resources.ingamePipeTop.texture);
                    this.mBotPipe = new PIXI.Sprite(Resources.ingamePipeBot.texture);
                    this.mTopPipe.scale.set(2.5);
                    this.mTopPipe.anchor.set(0.5, 1);
                    this.mBotPipe.anchor.set(0.5, 0);
                    this.mBotPipe.scale.set(2.5);
                    //
                    this.addChild(this.mTopPipe);
                    this.addChild(this.mBotPipe);
                }
            break;

            case this.STATE.RUN:
                this.isPassed = false;
                // console.log(this.position.x);
            break;

            case this.STATE.INACTIVE:
                // console.log('inactive');
            break;
        }
    }
}

module.exports = Pipes;