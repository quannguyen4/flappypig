const Input = require("../core/Input");
const EventDefine = require("../events/EventDefine");
const EventManager = require("../events/EventManager");
const BaseObject = require("./BaseObject");
const GameDefine = require('./GameDefine');

class Pig extends BaseObject
{
    constructor()
    {
        super();
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        this.mState = null;
        this.mObject = null;
        this.mIsFirstInit = false;
        this.mVelocity = 0;
        this.isUp = false;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                // this.mObject.update(dt);
                this._UpdatePosition(dt);
                // this._UpdateRotation(dt);
                this._UpdateFrame();
            break;
        }
    }

    TouchHandler(data)
    {
        let e = data.event;
        if (Input.IsTouchDown(e))
        {
            this.mVelocity = -GameDefine.BIRD_JUMP_VELOCITY;
            // this.rotation = -Math.PI/4;
            this.mObject.texture = Resources.ingamePigUp.texture;
            this.isUp = true;
        }
    }

    GetVelocity()
    {
        return this.mVelocity;
    }
    
    Died()
    {
        this.mObject.texture = Resources.ingamePigDie.texture;
    }

    _UpdatePosition(dt)
    {
        this.position.y = this.position.y + this.mVelocity*dt;
        this.mVelocity = this.mVelocity + GameDefine.GAME_GRAVITY*dt;
    }

    _UpdateRotation(dt)
    {
        this.rotation = this.rotation + 2*(Math.PI/4 - this.rotation)*dt;
    }

    _UpdateFrame()
    {
        if (this.mVelocity < 0 && !this.isUp)
        {
            this.mObject.texture = Resources.ingamePigUp.texture;
            this.isUp = true;
        }
        else if (this.mVelocity > 0 && this.isUp)
        {
            this.mObject.texture = Resources.ingamePig.texture;
            this.isUp = false;
        }
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                    // this.mObject = new PIXI.AnimatedSprite(Resources.birdAnim.frames);
                    this.mObject = new PIXI.Sprite(Resources.ingamePig.texture);
                    this.mObject.anchor.set(0.5);
                    //
                    this.mObject.scale.set(2.25*GameDefine.PAD_SCALE);
                    // this.mObject.animationSpeed = 0.25;
                    // this.mObject.loop = true;
                    //
                    this.addChild(this.mObject);

                    EventManager.subscribe(EventDefine.ON_TOUCH, this.TouchHandler.bind(this));
                }
                this.rotation = 0;
                // this.mObject.stop();
            break;

            case this.STATE.RUN:
                // this.mObject.gotoAndPlay(0);
            break;
        }
    }
}

module.exports = Pig;