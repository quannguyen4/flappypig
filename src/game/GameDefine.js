const Sample = require('./Sample');

class GameDefine
{
    constructor()
    {
        //CHEAT
        this.CHEAT_IMMORTAL                 = false;
        this.CHEAT_AUTO                     = false;

        //PHYSICS
        this.GAME_GRAVITY                   = 8000;

        //RESPONSIVE
        this.PAD_SCALE                      = 1;

        let n = 0;
        this.GAME_STATE_PRELOAD             = n++;
        this.GAME_STATE_LOADING             = n++;
        this.GAME_STATE_INGAME              = n++;
        this.GAME_STATE_END                 = n++;

        //FONT
        this.MAIN_FONT                      = {name: "Roboto", type: 'woff'};
        this.SUB_FONT_1                     = {name: "Bungee", type: 'truetype'};

        //TIME
        this.GAME_START_DELAY               = 0.5;

        //PIPE
        this.PIPE_MAX_SPAWN                 = 5;
        this.PIPE_DISTANCE                  = 800;
        this.PIPE_RANDOM_SPACE              = [450, 650];
        this.PIPE_RANDOM_Y                  = [-400, 400];

        //BIRD
        this.BIRD_BASE_SPEED                = 450; //Modify this
        this.BIRD_MOVE_SPEED                = this.BIRD_BASE_SPEED; //Don't modify this
        this.BIRD_SPEED_ACC                 = 20;
        this.BIRD_MAX_SPEED                 = 800;
        this.BIRD_JUMP_VELOCITY             = 1800;
        this.BIRD_SPAWN_X                   = 0;

        //ANIMATION
        n = 0;

        //WIN/LOSE/OTHER CONDITION
        n = 0;
        this.IS_GAME_WIN                    = n++;
        this.IS_GAME_LOSE                   = n++;

        //SCORE DEFINE
        this.GAME_SCORE_PER_JUMP            = 100;
        this.GAME_MAX_SCORE                 = 1000;

        //PARTICLE
        n = 0;
        this.PARTICLE_CLOUD                 = n++;

        //DECO
        n = 0;
        this.DECO_GROUND                    = n++;
        this.DECO_TREE                      = n++;
        this.DECO_TOP                       = n++;

        this.REWARD_CONFIG                  = null;
        this.LEADERBOARD_DATA               = null;
        this.REWARD_DATA                    = null;
        this.LEADERBOARD                    = {
            friends:{
                inMonth: {},
                allTime: {}
            },
            global:{
                inMonth: {},
                allTime: {}
            }
        };
        this.IS_MONTH                       = false;
        
        //
        this.KeyHandler();
    }

    SetApiInfo()
    {
        //API DEFINE
        this.MAIN_API_URL                   = 'https://m.dev.mservice.io';
        this.LEADERBOARD_PREFIX             = '?limit=100';
        this.LEADERBOARD_URL                = this.MAIN_API_URL + '/medalwall/v1/ranking/';
        this.REWARD_CONFIG_URL              = this.MAIN_API_URL + '/flappy-pig/v1/reward_config';
        this.POINT_SUBMIT_URL               = this.MAIN_API_URL + '/flappy-pig/v1/submit_point';
        this.HEADER_MAIN_CONFIG             = {
            'Authorization': `Bearer ${this.userToken}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };
    }

    GetLeaderboardData(isGlobal, inMonth)
    {
        if (isGlobal)
        {
            if (inMonth)
                return this.LEADERBOARD.global.inMonth;
            else
                return this.LEADERBOARD.global.allTime;
        }
        else
        {
            if (inMonth)
                return this.LEADERBOARD.friends.inMonth;
            else
                return this.LEADERBOARD.friends.allTime;
        }
    }

    /**
     * 
     * @param {CanvasContainer} container contain container width and height after resize, use this for responsive purpose 
     */
    Resize(container)
    {
        this.BIRD_SPAWN_X = 0.25*container.width;
        if (container.width > container.height)
        {
            if (container.width/container.height > 1.5)
                this.PAD_SCALE = 0.8*container.height/768;
            else
            {
                this.PAD_SCALE = 0.55*container.height/768;
                console.log(this.PAD_SCALE);
            }
        }
        else
            this.PAD_SCALE = 1;
    }

    KeyHandler()
    {
        document.addEventListener("keydown", function(event) {
            let d = document.getElementById('debugger');
            let s;
            if (!d)
            {
                console.log('no dd');
                d = document.createElement('div');
                d.id = 'debugger';
                d.style.position = 'fixed';
                d.style.top = 0;
                d.style.right = 0;
                document.body.appendChild(d);
            }
            if (event.keyCode === 73) {
                this.CHEAT_IMMORTAL = !this.CHEAT_IMMORTAL;
                if (d)
                {
                    s = document.createElement('span');
                    s.innerHTML = this.CHEAT_IMMORTAL ? 'You are immortal' : 'You can be die';
                    s.style.fontSize = '30px';
                    d.appendChild(s);
                    d.appendChild(document.createElement('br'));
                }
            }
            if (event.keyCode === 65) {
                this.CHEAT_AUTO = !this.CHEAT_AUTO;
                if (d)
                {
                    s = document.createElement('span');
                    s.innerHTML = this.CHEAT_AUTO ? 'Auto play Enabled' : 'Auto play disabled';
                    s.style.fontSize = '30px';
                    d.appendChild(s);
                    d.appendChild(document.createElement('br'));
                }
            }
            // do something
        }.bind(this));
    }

    AddDebugLog(t)
    {
        let d = document.getElementById('debugger');
        let s;
        if (!d)
        {
            console.log('no dd');
            d = document.createElement('div');
            d.id = 'debugger';
            d.style.position = 'fixed';
            d.style.top = '30px';
            d.style.right = '30px';
            document.body.appendChild(d);
        }
        if (d)
        {
            s = document.createElement('span');
            s.innerHTML = t.toString();
            s.style.fontSize = '50px';
            d.appendChild(s);
            d.appendChild(document.createElement('br'));
        }
    }

    isMobile()
    {
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
        {
            return true
        }
        else
        {
            return false
        }
    }
}

module.exports = new GameDefine();