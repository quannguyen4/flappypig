class BaseObject extends PIXI.Container
{
    constructor()
    {
        super();
        this.mState;
        this.mIsFirstInit = false;
    }
}

module.exports = BaseObject;