module.exports = {
    rank_sample: {
        "response_info": {
            "error_message": "success",
            "error_code": 0,
            "event_tracking": "v1_ranking"
        },
        "data": [
            {
                "userId": "0938877236",
                "userName": "Pham Minh Quoc",
                "point": 1900
            },
            {
                "userId": "0938877237",
                "userName": "Pham Minh Dep",
                "point": 1500
            },
            {
                "userId": "0938877238",
                "userName": "Pham Minh Trai",
                "point": 1000
            }
        ]
    },
    reward_sample:{
        "response_info": {
            "error_message": "success",
            "error_code": 0,
            "event_tracking": "v1_reward_config"
        },
        "data": [
            {
                "level": 1,
                "point": 200,
                "gift": "Thức ăn",
                "quantity": 200,
                "unit": "gam"
            },
            {
                "level": 2,
                "point": 500,
                "gift": "Thức ăn",
                "quantity": 500,
                "unit": "gam"
            },
            {
                "level": 3,
                "point": 1000,
                "gift": "Ngẫu nhiên",
                "quantity": 1,
                "unit": ""
            }
        ]
    },
    submit_sample: {
        "response_info": {
            "error_message": "success",
            "error_code": 0,
            "event_tracking": "v1_submit_point"
        },
        "data": [
            {
                "gameId": 1,
                "level": 200,
                "gift": "Thức ăn",
                "point": 200,
                "quantity": 200,
                "unit": "gam"
            },
            {
                "gameId": 1,
                "level": 500,
                "gift": "Thức ăn",
                "point": 500,
                "quantity": 500,
                "unit": "gam"
            }
        ]
    }
};