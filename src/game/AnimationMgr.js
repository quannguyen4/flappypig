const ObjectMgr = require('./ObjectMgr');
const GameDefine = require('./GameDefine');
// const PadClick = require('./Animations/PadClick');

class AnimationMgr
{
    constructor()
    {
        // super();
        let n = 0;
        this.mState = null;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        // this.mPadClicks = new ObjectMgr(PadClick);
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch(this.mState)
        {
            case this.STATE.RUN:
                // this.mPadClicks.Update(dt);
            break;
        }
    }

    GetAnimationContainer(c)
    {
        switch (c)
        {
            case GameDefine.ANIMATION_PAD_CLICK:
                // return this.mPadClicks;
        }
    }

    SpawnAnimation(animation, position)
    {
        let p;
        switch (animation)
        {
            case GameDefine.ANIMATION_PAD_CLICK:
                // p = this.mPadClicks._GetInstance();
            break;
        }
        p.Init();
        p.position.set(position.x, position.y);
        p.Start();
        // console.log('start');
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                // this.addChild(this.mPerfectParticles);
            break;

            case this.STATE.RUN:
                // console.log('start');
            break;
        }
    }
}

let a = new AnimationMgr();
a.Init();

module.exports = a;