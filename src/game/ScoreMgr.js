class ScoreMgr
{
    constructor()
    {
        let n = 0;
        this.mScore = 0;
        this.mState = null;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    AddScore(surplus)
    {
        this.mScore += surplus;
    }

    SetScore(score)
    {
        this.mScore = score;
    }

    GetScore()
    {
        return this.mScore;
    }

    Update(dt)
    {
        switch(this.mState)
        {
            case this.STATE.INIT:
            break;

            case this.STATE.RUN:
            break;
        }
    }

    //PRIVATE FUNCTION
    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                this.mScore = 0;
                this.Start();
            break;

            case this.STATE.RUN:
            break;
            
        }
    }
}

module.exports = new ScoreMgr();