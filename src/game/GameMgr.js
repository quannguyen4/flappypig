const ScoreMgr          = require('./ScoreMgr');
const Timer             = require('../core/Timer');
const APP               = require('../app');
const ParticleMgr       = require('./ParticleMgr');
const GameDefine        = require('./GameDefine');
const MainMusic         = require('./MainMusic');
const AnimationMgr      = require('./AnimationMgr');
const EventManager      = require('../events/EventManager');
const EventDefine       = require('../events/EventDefine');
const Pig               = require('./Pig');
const PipesMgr          = require('./PipesMgr');
const Utils             = require('../core/Utils');
const Pipes             = require('./Pipes');
const Sample            = require('./Sample');
const DecoMgr           = require('./DecoMgr');
const firstRank         = require('../assets/images/first_rank.png');
const secondRank        = require('../assets/images/second_rank.png');
const thirdRank         = require('../assets/images/third_rank.png');

class GameMgr extends PIXI.Container
{
    constructor()
    {
        super();
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++,
            TUTORIAL: n++,
            DELAY: n++,
            REPLAY_WAIT: n++,
        };
        this.mState;
        this.mIsFirstInit = false;
        this.mTimer = new Timer();
        this.mTimer.SetDuration(Infinity);
        this.mDelayStart = new Timer();
        this.mDelayStart.SetDuration(GameDefine.GAME_START_DELAY);
        //
        this.mScoreMgr = null;
        this.mPlayer = null;
        this.mTrackData = null;
        this.mDecoGround = new PIXI.Container();
        this.mDecoTree = new PIXI.Container();
        this.mDecoTop = new PIXI.Container();
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.TUTORIAL:
            break;

            case this.STATE.DELAY:
                if (this.mIsFirstInit)
                    this.mDelayStart.Update(dt);
                if (this.mDelayStart.IsDone())
                {
                    this.mDelayStart.Stop();
                    this.Start();
                }
            break;

            case this.STATE.RUN:
                this._UpdateFallOut(dt);
                this.mTimer.Update(dt);
                ParticleMgr.Update(dt);
                AnimationMgr.Update(dt);
                this.mPlayer.Update(dt);
                this.mPipesMgr.Update(dt);
                this.mDecoMgr.Update(dt);
                this._UpdateCollision(dt);
                this._UpdateMoveSpeed(dt);
            break;
        }
    }

    EventHandler()
    {
        EventManager.subscribe(EventDefine.TUTORIAL_VIEW_END, function(){
            // this._SetState(this.STATE.DELAY);
            this.Start();
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_TOUCH, function(){
            // this._SetState(this.STATE.DELAY);
            if (this.mState == null && GameDefine.REWARD_DATA)
            {
                EventManager.publish(EventDefine.ON_WAIT_RESTART, {});
                this._SetState(this.STATE.REPLAY_WAIT);
            }
            else if (this.mState == this.STATE.REPLAY_WAIT)
            {
                EventManager.publish(EventDefine.ON_GAME_RESTART, {});
                this.Start();
                ScoreMgr.SetScore(0);
            }
        }.bind(this));

        document.getElementById('btnReplayEndCard').onclick = function(){
            if (this.mState == null && GameDefine.REWARD_DATA)
            {
                hideDialog();
                this._SetState(this.STATE.REPLAY_WAIT);
            }
        }.bind(this);

        EventManager.subscribe(EventDefine.ON_SCORE_SUCCESS, function(d){
            ScoreMgr.AddScore(GameDefine.GAME_SCORE_PER_JUMP);
            d.data = [Math.round(d.data[0]), Math.round(d.data[1])];
            // console.log(ScoreMgr.GetScore().toString().length + ScoreMgr.GetScore().toString());
            let pointReverse = ScoreMgr.GetScore().toString().split('').reverse().join('');
            let formatData = {
                player: Utils.FormatNumber(Math.round(this.mPlayer.getGlobalPosition().y), 4),
                pipe: `${Utils.FormatNumber(d.data[0], 4)}${Utils.FormatNumber(d.data[1], 4)}`,
                velo: Utils.FormatNumber(Math.round(this.mPlayer.GetVelocity()), 4),
                point: `${ScoreMgr.GetScore().toString().length}${pointReverse}`,
                // velo: 0
            };
            let data = `${formatData.player}${formatData.pipe}${formatData.velo}${formatData.point}`;
            if (this.mTrackData.length == 0) this.mTimer.Resume();
            this.mTrackData.push(data);
            let s = this.mTrackData.join('');
            s = s.replaceAll('"', '');
            this.ValidateData(s)
        }.bind(this));
    }

    Responsive()
    {
        if (APP.GetWidth() > APP.GetHeight())
        {

        }
        else
        {
            this.mPlayer.position.set(GameDefine.BIRD_SPAWN_X, 0.5*APP.GetHeight());
            this.mPipesMgr.position.set(1.5*APP.GetWidth(), 0.5*APP.GetHeight());
            this.mDecoGround.position.set(0.5*APP.GetWidth(), APP.GetHeight());
            this.mDecoTree.position.set(0.5*APP.GetWidth(), APP.GetHeight() - 0.75*this.mDecoGround.height);
            this.mDecoTop.position.set(0.5*APP.GetWidth(), 0);
        }
    }

    ValidateData(strData)
    {
        let strCount = 4;
        let dataCount = 4;
        //
        let deserialize = (function(){
            let arr = [];
            let curCount = 0;
            let curStr = '';
            for (let i = 0; i< strData.length; i++)
            {
                let str = strData.charAt(i);
                curStr = curStr + str;
                if (str == '-')
                    continue;
                curCount++;
                if (curCount == strCount * dataCount)
                {
                    let pointCount = parseInt(strData.charAt(i + 1));
                    let point = (function(){
                        let arr = [];
                        for (let j = 0; j< pointCount; j++)
                            arr.push(strData.charAt(i + 2 + j));
                        return arr;
                    }()).reverse().join('');
                    arr.push(curStr + point);
                    curStr = '';
                    curCount = 0;
                    i += pointCount + 1;
                    if (i >= strData.length) break;
                }
            }
            return arr;
        }());
        // console.log(deserialize);
        if (deserialize.length > 1)
        {
            let countVelo = 0;
            for (let i = 1; i< deserialize.length; i++)
            {
                let d = (function(){
                    let arr = [];
                    let curCount = 0;
                    let curStr = '';
                    for (let j = 0; j< deserialize[i].length; j++)
                    {
                        let str = deserialize[i].charAt(j);
                        curStr = curStr + str;
                        if (str == '-')
                            continue;
                        curCount++;
                        if (curCount == strCount && arr.length < dataCount)
                        {
                            arr.push(curStr);
                            curStr = '';
                            curCount = 0;
                        }
                        if (arr.length == dataCount)
                        {
                            arr.push(deserialize[i].substring(j + 1, deserialize[i].length));
                            break;
                        }
                    }
                    return arr;
                }());
                deserialize[i] = d;
                let bird = parseInt(d[0]);
                let pipe = [parseInt(d[1]), parseInt(d[2])];
                let velocity = parseInt(d[3]);
                if (velocity == 0)
                    countVelo++;
                if (
                    bird < pipe[0] 
                    || bird > pipe[1] 
                    || pipe.length != 2
                    || countVelo >= 2
                )
                    return false;
            }
            let point = parseInt(deserialize[deserialize.length - 1][deserialize[deserialize.length - 1].length - 1]);
            if (point != 'Pass point to here')
                return false;
        }
        return true;
    }

    async SubmitPoint()
    {
        let returnValue = null;
        let path = [];
        if (this.mTrackData.length > 20)
        {
            let l = this.mTrackData.slice(Math.floor(this.mTrackData.length/2 - 10), Math.floor(this.mTrackData.length/2));
            let r = this.mTrackData.slice(Math.floor(this.mTrackData.length/2), Math.floor(this.mTrackData.length/2 + 10));
            path = [...l, ...r];
        }
        let s = path.join('');
        s = s.replaceAll('"', '');
        let data = {
            "gameId": "123",
            "path": s,
            "point": ScoreMgr.GetScore(),
            "hash": "BA7816BF8F01CFEA414140DE5DAE2223B00361A396177A9CB410FF61F20015AD",
            "eventTime": 1615449667000
        };
        await fetch(GameDefine.POINT_SUBMIT_URL, 
        {
            method: 'POST',
            credentials: 'include',
            withCredentials: true,
            headers: new Headers({
                ...GameDefine.HEADER_MAIN_CONFIG
            }),
            body: JSON.stringify(data),
        }).then(response => response.json())
        .then(function(res){
            returnValue = res;
        })
        .catch(function(error) {
            let s = Sample.submit_sample;
            returnValue = s;
            return s;
        });
        
        GameDefine.REWARD_DATA = returnValue;
        this.ShowReward(GameDefine.REWARD_DATA.data);
    }

    ShowReward(data)
    {
        let parent = document.getElementById('prizeRecieve');
        parent.innerHTML = '';
        if (data.length == 0)
        {
            document.getElementById('congratText').style.display = 'none';
            document.getElementById('btnReplayEndCard').style.marginTop = '0';
        }
        else
        {
            document.getElementById('congratText').style.display = 'block';
            document.getElementById('btnReplayEndCard').style.marginTop = '12%';
        }
        for (let i =0; i< data.length; i++)
        {
            let el = document.getElementById('rewardItem').cloneNode(true);
            el.id = null;
            el.style.display = null;
            el.getElementsByClassName('rewardPoint')[0].remove();
            el.getElementsByClassName('rewardQuantity')[0].innerHTML = data[i].quantity + ' ' + data[i].unit;
            el.getElementsByClassName('rewardCaption')[0].innerHTML = data[i].gift;
            parent.appendChild(el);
        }
    }

    async LoadLeaderboard(isGlobal = true, inMonth = true)
    {
        let d = GameDefine.GetLeaderboardData(isGlobal, inMonth);
        let returnValue = null;
        await fetch(`${GameDefine.LEADERBOARD_URL}${isGlobal ? 'global' : 'friends'}${GameDefine.LEADERBOARD_PREFIX}&${inMonth ? 'medal_types=flappypig_point_month' : 'medal_types=flappypig_point'}`,
        {
            method: 'GET',
            credentials: 'include',
            withCredentials: true,
            headers: new Headers({
                ...GameDefine.HEADER_MAIN_CONFIG
            }),
        }).then(response => response.json())
        .then(function(res) {
            returnValue = res;
        })
        .catch(function(error) {
            return error;
        });
        
        d = returnValue;
        for (let a in d)
            GameDefine.GetLeaderboardData(isGlobal, inMonth)[a] = d[a];
        GameDefine.LEADERBOARD_DATA = d;
        this.ShowLeaderboard(d.items);
    }

    ShowLeaderboard(data)
    {
        let parent = document.getElementById('endCardRank');
        let curProfile = document.getElementById('currentProfileCard');
        curProfile.getElementsByClassName('rankImg')[0].innerHTML = '';
        curProfile.getElementsByClassName('rankImg')[0].style.background = '';
        curProfile.getElementsByClassName('rankImg')[0].classList.remove('rankImgText');
        parent.innerHTML = '';
        for (let i =0; i< data.length; i++)
        {
            let el = document.getElementById('profileCardItem').cloneNode(true);
            el.id = null;
            el.style.display = null;
            el.getElementsByClassName('profileName')[0].innerHTML = data[i].name;
            el.getElementsByClassName('profilePoint')[0].innerHTML = data[i].point;
            parent.appendChild(el);
        }
        curProfile.getElementsByClassName('profileName')[0].innerHTML = GameDefine.LEADERBOARD_DATA.meta.name;
        curProfile.getElementsByClassName('profilePoint')[0].innerHTML = GameDefine.LEADERBOARD_DATA.meta.point;
        if (GameDefine.LEADERBOARD_DATA.meta.rank <= 3)
        {
            curProfile.getElementsByClassName('rankImg')[0].classList.remove('rankImgText');
            switch (GameDefine.LEADERBOARD_DATA.meta.rank)
            {
                case 1:
                    curProfile.getElementsByClassName('rankImg')[0].style.background= `transparent url(${firstRank.default}) no-repeat center`;
                break;

                case 2:
                    curProfile.getElementsByClassName('rankImg')[0].style.background= `transparent url(${secondRank.default}) no-repeat center`;
                break;

                case 3:
                    curProfile.getElementsByClassName('rankImg')[0].style.background= `transparent url(${thirdRank.default}) no-repeat center`;
                break;
            }
        }
        else
        {
            curProfile.getElementsByClassName('rankImg')[0].classList.add('rankImgText');
            curProfile.getElementsByClassName('rankImg')[0].innerHTML = GameDefine.LEADERBOARD_DATA.meta.rank;
        }
        renderRankDOM();
    }

    _Restart()
    {
        document.getElementById('finalPoint').innerHTML = ScoreMgr.GetScore() + '';
        showDialog('prizeReward');
        this.SubmitPoint();
        this.LoadLeaderboard();
        this.mPlayer.Died();
        this._SetState(null);
    }

    _UpdateCollision(dt)
    {
        if (GameDefine.CHEAT_IMMORTAL)
            return;
        let player = this.mPlayer;
        let pipeCon = this.mPipesMgr.GetObjectArr();
        for (let i = 0; i< pipeCon.length; i++)
        {
            /**
             * @type {Pipes}
             */
            let pipes = pipeCon[i];
            if (pipes.IsInactive())
                continue;
            for (let j = 0; j< pipes.children.length; j++)
            {
                /**
                 * @type {PIXI.Sprite}
                 */
                let pipe = pipes.children[j];
                let playerBound = {
                    x: player.getBounds().x + 60,
                    y: player.getBounds().y + 60,
                    width: player.getBounds().width - 120,
                    height: player.getBounds().height - 120
                };
                if (Utils.Collision2Rect(pipe.getBounds(), playerBound))
                {
                    this._Restart();
                    // let e = new CustomEvent('gamePause', {});
                    // window.dispatchEvent(e);
                }
            }
        }
    }

    _UpdateFallOut(dt)
    {
        if (this.mPlayer.getGlobalPosition().y + 0.5*this.mPlayer.height > this.mDecoGround.getGlobalPosition().y - this.mDecoGround.height)
        {
            this.mPlayer.position.set(this.mPlayer.position.x, this.mDecoGround.position.y - this.mDecoGround.height - 0.5*this.mPlayer.height);
            this._Restart();
        }
        if (this.mPlayer.getGlobalPosition().y - 0.5*this.mPlayer.height < this.mDecoTop.getGlobalPosition().y + 0.5*this.mDecoTop.height)
        {
            this.mPlayer.position.set(this.mPlayer.position.x, this.mDecoTop.position.y + 0.5*this.mDecoTop.height + 0.5*this.mPlayer.height);
            this._Restart();
        }
    }

    _UpdateMoveSpeed(dt)
    {
        GameDefine.BIRD_MOVE_SPEED = Math.min(GameDefine.BIRD_MAX_SPEED, GameDefine.BIRD_MOVE_SPEED + GameDefine.BIRD_SPEED_ACC*dt);
    }

    //PRIVATE FUNCTION
    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                    this.mScoreMgr = ScoreMgr;
                    this.mPlayer = new Pig();
                    this.mPipesMgr = new PipesMgr();
                    this.mDecoMgr = new DecoMgr();
                    //
                    this.mScoreMgr.Init();
                    this.mPlayer.Init();
                    this.mPipesMgr.Init();
                    this.mDecoMgr.Init();
                    //
                    this.mDecoGround.addChild(...this.mDecoMgr.GetDeco(GameDefine.DECO_GROUND));
                    this.mDecoTree.addChild(...this.mDecoMgr.GetDeco(GameDefine.DECO_TREE));
                    this.mDecoTop.addChild(...this.mDecoMgr.GetDeco(GameDefine.DECO_TOP));
                    //
                    this.Responsive();
                    this.addChild(ParticleMgr.GetParticleContainer(GameDefine.PARTICLE_CLOUD));
                    this.addChild(this.mDecoTree);
                    this.addChild(this.mPipesMgr);
                    this.addChild(this.mDecoTop);
                    this.addChild(this.mPlayer);
                    this.addChild(this.mDecoGround);
                    this.EventHandler();
                    
                    for (let i = 0; i< 3; i++)
                    {
                        ParticleMgr.SpawnParticle(GameDefine.PARTICLE_CLOUD, {x: Utils.Rand(0, APP.GetWidth()), y: Utils.Rand(200, APP.GetHeight() - 200)});
                    }
                }
                this._SetState(this.STATE.TUTORIAL);
            break;

            case this.STATE.RUN:
                this.mTimer.Pause();
                this.mTimer.Reset();
                this.mPlayer.Start();
                this.mPipesMgr.Start();
                this.mDecoMgr.Start();
                this.mTrackData = [];
            break;

            case this.STATE.DELAY:
                this.mDelayStart.RegisterTimestampCallback(GameDefine.GAME_SONG_START_TIME - GameDefine.PAD_MOVE_TIME, function(){
                    // console.log('call');
                }.bind(this));
            break;

            case this.STATE.TUTORIAL:
                ParticleMgr.Start();
                AnimationMgr.Start();
            break;

            case this.STATE.REPLAY_WAIT:
                // document.getElementById('finalPoint').innerHTML = ScoreMgr.GetScore() + '';
                // showDialog('prizeReward');
                // this.SubmitPoint();
                // this.LoadLeaderboard();
                this.mPlayer.position.set(GameDefine.BIRD_SPAWN_X, 0.5*APP.GetHeight());
                this.mPipesMgr.position.set(1.5*APP.GetWidth(), 0.5*APP.GetHeight());
                this.mPlayer.Init();
                this.mPipesMgr.Init();
                GameDefine.BIRD_MOVE_SPEED = GameDefine.BIRD_BASE_SPEED;
            break;
        }
    }
}

module.exports = GameMgr;