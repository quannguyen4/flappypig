const Utils = require("../core/Utils");
const GameDefine = require("./GameDefine");
const ObjectMgr = require("./ObjectMgr");
const Pipes = require('./Pipes');

class PipesMgr extends ObjectMgr
{
    constructor()
    {
        super(Pipes);
        this.mState = null;
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        this.mIsFirstInit = false;
        this.mMaxSpawn = GameDefine.PIPE_MAX_SPAWN;
        this.mPipeDistance = GameDefine.PIPE_DISTANCE;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                super.Update(dt);
                // console.log(this.mObjArr[0]);
                this._UpdateSpawnPipes(dt);
            break;
        }
    }

    _UpdateSpawnPipes(dt)
    {
        for (let i = 0; i< this.mMaxSpawn; i++)
        {
            /**
             * @type {Pipes}
             */
            let pipes = this.mObjArr[i];
            if (pipes.IsInactive())
            {
                // pipes.position.set(this._GetLastPipePosition() == 0 ? 0 : this._GetLastPipePosition() + this.mPipeDistance, 0);
                pipes.position.set(
                    this._GetLastPipePosition() == 0 ? 0 : this._GetLastPipePosition() + this.mPipeDistance, 
                    Utils.Rand(GameDefine.PIPE_RANDOM_Y[0], GameDefine.PIPE_RANDOM_Y[1])
                );
                pipes.SetPipeSpace(Utils.Rand(GameDefine.PIPE_RANDOM_SPACE[0], GameDefine.PIPE_RANDOM_SPACE[1]));
                pipes.Start();
            }
        }
    }

    _GetLastPipePosition()
    {
        let max = 0;
        for (let i = 0; i< this.mObjArr.length; i++)
        {
            if (this.mObjArr[i].IsInactive())
                continue;
            if (this.mObjArr[i].position.x > max)
                max = this.mObjArr[i].position.x;
        }
        return max;
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                }
                this.mObjArr.length = 0;
                this.removeChildren();
                for (let i = 0; i< this.mMaxSpawn; i++)
                {
                    /**
                     * @type {Pipes}
                     */
                    let pipes = this._GetInstance();
                    pipes.Init();
                    pipes.position.set(i * this.mPipeDistance, i == 0 ? 0 : Utils.Rand(GameDefine.PIPE_RANDOM_Y[0], GameDefine.PIPE_RANDOM_Y[1]));
                    pipes.SetPipeSpace(i == 0 ? 600 : Utils.Rand(GameDefine.PIPE_RANDOM_SPACE[0], GameDefine.PIPE_RANDOM_SPACE[1]));
                    // console.log(pipes.position.x);
                }
            break;

            case this.STATE.RUN:
                for (let i = 0; i< this.mObjArr.length; i++)
                {
                    this.mObjArr[i].Start();
                }
            break;
        }
    }
}

module.exports = PipesMgr;