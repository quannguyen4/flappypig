class Button extends PIXI.Container
{
    constructor(iconTexture)
    {
        super();
        let lineWidth = 6;
        this.mBackground = new PIXI.Graphics()
        .lineStyle(lineWidth, 0xffffff, 1)
        .beginFill(0x38585f, 0.8)
        .drawRoundedRect(0, 0, 100, 100, 30)
        .endFill();
        this.mForeground = new PIXI.Sprite(iconTexture);
        this.mForeground.anchor.set(0.5);
        let padding = 0.22;
        //
        this.mBackground.scale.set(this.mForeground.width/this.mBackground.width + padding, this.mForeground.height/this.mBackground.height + padding);
        this.mBackground.position.set(-0.5* this.mBackground.width + lineWidth/4, -0.5* this.mBackground.height + lineWidth/4);
        //
        this.addChild(this.mBackground, this.mForeground);
    }

    setIcon(texture)
    {
        let lineWidth = 6;
        this.mForeground.texture = texture;
        this.mBackground.scale.set(this.mForeground.width/this.mBackground.width, this.mForeground.height/this.mBackground.height);
        this.mBackground.position.set(-0.5* this.mBackground.width + lineWidth/4, -0.5* this.mBackground.height + lineWidth/4);
    }
}

module.exports = Button;