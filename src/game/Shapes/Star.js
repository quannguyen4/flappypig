const Utils = require("../../core/Utils");

const defaultOpt = {
    x: 0,
    y: 0,
    points: 3,
    outerRadius: 50,
    innerRadius: 25,
    angle: 0
};

class Star extends PIXI.Graphics
{
    constructor(options)
    {
        if (!options)
            options = defaultOpt;
        super();
        //
        let n = 0;
        this.mState = null;
        this.STATE = {
            RUN: n++,
            INACTIVE: n++
        }
        this._Draw(options);
    }

    _Draw(options)
    {
        options.points = Utils.RandInt(3, 5);
        if (options.points > 2) 
        {
            // init vars
            // options.points = Utils.RandInt(2, 5);
            let step, halfStep, start, dx, dy;
            step = (Math.PI * 2) / options.points;
            halfStep = step / 2;
            start = (options.angle / 180) * Math.PI;
            this.lineStyle(1, 0x94217b, 0.7);
            this.moveTo(options.x + (Math.cos(start) * options.outerRadius), options.y - (Math.sin(start) * options.outerRadius));
            for (let n = 1; n <= options.points; ++n) {
                dx = options.x + Math.cos(start + (step * n) - halfStep) * options.innerRadius;
                dy = options.y - Math.sin(start + (step * n) - halfStep) * options.innerRadius;
                this.lineTo(dx, dy);
                dx = options.x + Math.cos(start + (step * n)) * options.outerRadius;
                dy = options.y - Math.sin(start + (step * n)) * options.outerRadius;
                this.lineTo(dx, dy);
            }
        }
    }

    Update(dt)
    {
        switch(this.mState)
        {
            case this.STATE.RUN:
                this._UpdateScale(dt);
                this._UpdateRotation(dt);
                this._UpdateAlpha(dt);
                this._UpdateInactive();
            break;
            
            case this.STATE.INACTIVE:
            break;
        }
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    IsInactive()
    {
        return this.mState == this.STATE.INACTIVE;
    }

    _UpdateScale(dt)
    {
        // this.mBaseScale += 2 * dt;
        // this.scale.set(this.mBaseScale);
        this.mDeltaScale += 0.5 * dt;
        this.scale.x += 2 * dt;
        this.scale.y = Math.cos(this.mDeltaScale) * this.mBaseScale;
        // console.log(this.scale.x);
        if (this.mDeltaScale >= 2*Math.PI)
            this.mDeltaScale = 0;
    }

    _UpdateRotation(dt)
    {
        this.rotation += 1 * this.mDir * dt;
    }

    _UpdateAlpha(dt)
    {
        this.mDeltaAlpha = Math.min(3*Math.PI, this.mDeltaAlpha + 2 * dt);
        this.alpha = 0.5*Math.cos(this.mDeltaAlpha) + 0.5;
    }

    _UpdateInactive()
    {
        if (this.mDeltaAlpha == 3*Math.PI)
            this._SetState(this.STATE.INACTIVE);
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.RUN:
                this.mBaseScale = Utils.Rand(8, 15);
                this.mDeltaScale = 0;
                this.scale.set(this.mBaseScale);
                this.alpha = 0;
                this.mDeltaAlpha = Math.PI;
                this.mDir = (function(){
                    let a = [-1 , 1];
                    return a[Utils.RandInt(0, a.length - 1)];
                }());
                this.visible = true;
            break;

            case this.STATE.INACTIVE:
                this.visible = false;
            break;
        }
    }
}

module.exports = Star;