const APP = require('../app');
const GameDefine = require('./GameDefine');

class DecoMgr extends PIXI.Container
{
    constructor()
    {
        super();
        this.mState = null;
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++,
        };
        this.mGrounds = [];
        this.mTrees = [];
        this.mTops = [];
        this.mIsFirstInit = false;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                this._UpdateGround(dt);
                this._UpdateTree(dt);
                this._UpdateTop(dt);
            break;
        }
    }

    GetDeco(d)
    {
        switch (d)
        {
            case GameDefine.DECO_GROUND:
                return this.mGrounds;
            case GameDefine.DECO_TREE:
                return this.mTrees;
            case GameDefine.DECO_TOP:
                return this.mTops;
        }
    }

    _UpdateGround(dt)
    {
        for (let i = 0; i< this.mGrounds.length; i++)
        {
            this.mGrounds[i].position.x = this.mGrounds[i].position.x - GameDefine.BIRD_MOVE_SPEED*dt;
            if (this.mGrounds[i].position.x < -((this.mGrounds.length/2 - 0.5)*this.mGrounds[i].width))
            {
                this.mGrounds[i].position.x += 2*this.mGrounds[i].width;
            }
        }
    }

    _UpdateTree(dt)
    {
        for (let i = 0; i< this.mTrees.length; i++)
        {
            this.mTrees[i].position.x = this.mTrees[i].position.x - 0.25*GameDefine.BIRD_MOVE_SPEED*dt;
            if (this.mTrees[i].position.x < -((this.mTrees.length/2 - 0.5)*this.mTrees[i].width))
            {
                this.mTrees[i].position.x += 2*this.mTrees[i].width;
            }
        }
    }

    _UpdateTop(dt)
    {
        for (let i = 0; i< this.mTops.length; i++)
        {
            this.mTops[i].position.x = this.mTops[i].position.x - GameDefine.BIRD_MOVE_SPEED*dt;
            if (this.mTops[i].position.x < -((this.mTops.length/2 - 0.5)*this.mTops[i].width))
            {
                this.mTops[i].position.x += 2*this.mTops[i].width;
            }
        }
    }

    _SetState(s)
    {
        this.mState = s;
        switch (this.mState)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                    for (let i = 0; i< 3; i++)
                    {
                        let ground = new PIXI.Sprite(Resources.ingameGround.texture);
                        ground.anchor.set(0.5, 1);
                        ground.scale.set(2, 2);
                        ground.position.set((i + 0.5 - 3/2)*ground.width, 0);
                        this.mGrounds.push(ground);
                    }
                    for (let i = 0; i< 3; i++)
                    {
                        let tree = new PIXI.Sprite(Resources.ingameTreeDeco.texture);
                        tree.anchor.set(0.5, 1);
                        tree.scale.set(2);
                        tree.position.set((i + 0.5 - 3/2)*tree.width, 0);
                        this.mTrees.push(tree);
                    }
                    for (let i = 0; i< 3; i++)
                    {
                        let top = new PIXI.Sprite(Resources.ingameTopDeco.texture);
                        top.anchor.set(0.5, 0);
                        top.scale.set(2);
                        top.position.set((i + 0.5 - 3/2)*top.width, 0);
                        this.mTops.push(top);
                    }
                }
            break;

            case this.STATE.RUN:
            break;
        }
    }
}

module.exports = DecoMgr;