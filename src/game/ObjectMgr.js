class ObjectMgr extends PIXI.Container
{
    /**
     * 
     * @param {ObjectType} instance pass object type to pool
     */
    constructor(instance)
    {
        super();
        this.mObjArr = [];
        this.mInstance = instance;
    }

    //PRIVATE FUNCTION
    _GetInstance()
    {
        for (let i = 0; i< this.mObjArr.length; i++)
        {
            if (this.mObjArr[i].IsInactive())
                return this.mObjArr[i];
        }
        let instance = new (this.mInstance)();
        this.mObjArr.push(instance);
        this.addChild(instance);
        return instance;
    }

    Update(dt)
    {
        /* Add custom code here */
        //////////////////////////////////////
        //////////////////////////////////////
        //Private Update (it's a must)
        this._Update(dt);
    }

    GetObjectArr()
    {
        return this.mObjArr;
    }

    _Update(dt)
    {
        if (this.mObjArr.length == 0)
            return;
        for (let i = 0; i< this.mObjArr.length; i++)
            this.mObjArr[i].Update(dt);
    }
    
}

module.exports = ObjectMgr;