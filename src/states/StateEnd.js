const BaseState = require('./BaseState');
const View = require('../views/StateEndView');
const APP = require('../app');
const Utils = require('../core/Utils');

class StateEnd extends BaseState {
    constructor() {
        super();
        //
        this.mView = new View();
        // this.addChild(this.mView);
        this.mState;
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++,
        };
    }

    Prelease() 
    {
        APP.stage.addChild(this.mView);
        if (!this.mIsFirstInit) 
        {
            this.mIsFirstInit = true;
            //
            // console.log(PIXI.Loader.shared);
            let assets = [];
            // assets.push({name: "gameBG", url: Utils.GetAssetUrl("bg.jpg")});
            // assets.push({name: "test_ms", url: Utils.GetAssetUrl("sound_test.mid")});
            //
            Utils.LoadAssets(assets, this.LoadCompleteHandler.bind(this), this.LoadProgressHandler.bind(this), this.LoadErrorhandler.bind(this));

        }
        else
            this.mView.Init();
    }

    Release() {
        // this.SetTouchable(false);
        super.Release();
    }

    Init() {
        // PIXI.loader.removeAllListeners();
        // PIXI.loader.reset();
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        if (this.mView.alpha == 1)
            this.mView.Update(dt);
    }

    LoadProgressHandler(loader, res) 
    {
        
    }

    LoadCompleteHandler(loader, res) {
        // Utils.LoadFont('DancingScript' , (require('../fonts/DancingScript-Regular.ttf')).default);
        this.Init();
    }

    LoadErrorhandler(loader, res) {

    }

    //PRIVATE FUNCTION
    _SetState(s)
    {
        this.mState = s;
        switch (s) {
            case this.STATE.INIT:
                // this.mView.SetResources(loader.resources);
                this.mView.Init();
                // this.mGameMgr.Init();
                //
                // this.mView.GameView = this.mGameMgr;
            break;

            case this.STATE.RUN:
            break;
        }
    }
}

module.exports = StateEnd;