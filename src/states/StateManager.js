const APP                   = require('../app');
const GameDefine = require('../game/GameDefine');

class StateManager
{
    constructor()
    {
        this.isInstantiated = false;
        let n = 0;
        this.mState = null;
        this.STATE = {
            INIT: n++,
            RUN: n++,
            TRANSITION: n++,
            PAUSE: n++,
            END: n++
        }
        this.mStateStack = [];
        this.mCurrentState;
        this.mNextState;
        this.isRun = false;
    }

    Init()
    {
        // console.log('init');
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    AddState(s)
    {
        if (Array.isArray(s))
            this.mStateStack.push(...s);
        else
            this.mStateStack.push(s);
    }

    RemoveState(s)
    {
        if (this.mStateStack[s])
            this.mStateStack.splice(this.mStateStack.indexOf(s), 1);
    }

    SwitchState(s, transition = false)
    {
        if (transition)
        {
            // console.log(s);
            this.mNextState = s;
            this._SetState(this.STATE.TRANSITION);
            // console.log('switch');
            return;
        }
        // console.log(this.mStateStack[this.mCurrentState]);
        if (this.mCurrentState > -1)
            this.mStateStack[this.mCurrentState].Release();
        this.mCurrentState = this.mNextState;
        this.mStateStack[this.mCurrentState].Prelease();
        // if (this.mStateStack.indexOf(this.mCurrentState) != -1)
        // {
        //     this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)].Release();
        // }
        // this.mCurrentState = this.mStateStack[this.mNextState];
        // // console.log(this.mCurrentState);
        // this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)].Prelease();
    }

    NextState(transition = false)
    {
        if (transition)
        {
            this.mNextState = this.mStateStack.indexOf(this.mCurrentState) + 1;
            this._SetState(this.STATE.TRANSITION);
            return;
        }
        this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)].Release();
        this.mCurrentState = this.mStateStack[this.mNextState];
        this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)].Prelease();
    }

    Pause()
    {
        // this.mStateStack[this.mCurrentState].Pause();
        if (!this.mStateStack[this.mCurrentState].isFadeComplete)
            this._SetState(this.STATE.TRANSITION);
        else
        {
            this._SetState(this.STATE.PAUSE);
        }
    }

    Resume()
    {
        // this.mStateStack[this.mCurrentState].Resume();
        if (this.mState != this.STATE.TRANSITION)
        {
            this.isRun = true;
            setTimeout(function(){
                if (!this.isRun)
                    return;
                this._SetState(this.STATE.RUN);
            }.bind(this), 200);
        }
    }

    //Update Handler
    Update(dt)
    {
        // console.log(this.mState);
        switch (this.mState) 
        {
            case this.STATE.TRANSITION:
                if (this.mCurrentState > -1)
                    this.mStateStack[this.mCurrentState].Update(dt);
                if (this.mCurrentState > -1)
                    this.mStateStack[this.mCurrentState].FadeOut(dt);
                // console.log(this.mCurrentState);
                if (this.mCurrentState == -1 || this.mStateStack[this.mCurrentState].isFadeComplete)
                {
                    // console.log(this.mCurrentState);
                    this._SetState(this.STATE.RUN);
                    this.SwitchState(this.mNextState, false);
                }
                // if (this.mCurrentState)
                //     this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)].Update(dt);
                // if (!this.mCurrentState || this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)].isFadeComplete)
                // {
                //     // if (this.mCurrentState) 
                //     //     console.log(this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)]);
                //     // throw new Error('fake');
                //     this._SetState(this.STATE.RUN);
                //     this.SwitchState(this.mNextState, false);
                //     this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)].alpha = 0;
                // }
            break;

            case this.STATE.RUN:
                if (this.mCurrentState > -1)
                    this.mStateStack[this.mCurrentState].Update(dt);
                if (this.mCurrentState >-1 )
                    this.mStateStack[this.mCurrentState].FadeIn(dt);
            break;

            case this.STATE.PAUSE:
            break;
        }
    }

    //PRIVATE FUNCTION

    //State Handler
    _SetState(s)
    {
        if (this.mState == this.STATE.PAUSE)
        {
            if (s == this.STATE.RUN)
                this.mStateStack[this.mCurrentState].Resume();
        }
        this.mState = s;
        switch(this.mState)
        {
            case this.STATE.INIT:
                if (this.isInstantiated == false)
                {
                    // this.mStateStack = [];
                    this.mCurrentState = -1;
                    this.SwitchState(0, true);
                    // this._SetState(this.STATE.TRANSITION);
                    window.addEventListener('gamePause', this.Pause.bind(this));
                    window.addEventListener('gameResume', this.Resume.bind(this));
                    APP.ticker.add(function() {
                        let deltaTime = APP.ticker.elapsedMS / 1000;
                        this.Update(deltaTime);
                    }.bind(this));
                    this.isInstantiated = true;
                }
                else
                    this.Start();
            break;

            case this.STATE.TRANSITION:
                // this.mStateStack[this.mStateStack.indexOf(this.mCurrentState)].Release();
            break;

            case this.STATE.RUN:
                this.isRun = true;
            break;

            case this.STATE.END:
            break;

            case this.STATE.PAUSE:
                this.isRun = false;
                this.mStateStack[this.mCurrentState].Pause();
            break;
        }
    }
}

module.exports = new StateManager();