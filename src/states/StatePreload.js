const BaseState = require('./BaseState');
const View = require('../views/StatePreloadView');
const APP = require('../app');
const Utils = require('../core/Utils');
const GameDefine = require('../game/GameDefine');
const MainMusic = require('../game/MainMusic');
const StateManager = require('./StateManager');
const bird = require('../assets/images/bird.json');
const Buffer = require('buffer/').Buffer;

class StatePreload extends BaseState
{
    constructor()
    {
        super();
        // console.log(new StateManager);
        //
        this.mView = new View();
        this.mIsAssetLoad = false;
        this.mIsMusicLoad = false;
    }

    Prelease()
    {
        // console.log('add');
        APP.stage.addChild(this.mView);
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            let assets = [];
            assets.push({name: 'ingameBg', url: Utils.GetAssetUrl('images/bg.png')});
            assets.push({name: 'ingameGround', url: Utils.GetAssetUrl('images/ground.png')});
            assets.push({name: 'ingamePipeTop', url: Utils.GetAssetUrl('images/pipetop.png')});
            assets.push({name: 'ingamePipeBot', url: Utils.GetAssetUrl('images/pipebot.png')});
            assets.push({name: 'ingameTreeDeco', url: Utils.GetAssetUrl('images/treedeco.png')});
            assets.push({name: 'ingameTopDeco', url: Utils.GetAssetUrl('images/topdeco.png')});
            assets.push({name: 'ingamePig', url: Utils.GetAssetUrl('images/pig.png')});
            assets.push({name: 'ingamePigUp', url: Utils.GetAssetUrl('images/pigup.png')});
            assets.push({name: 'ingamePigDie', url: Utils.GetAssetUrl('images/pigdie.png')});
            assets.push({name: 'ingameCloud', url: Utils.GetAssetUrl('images/cloud.png')});
            assets.push({name: 'scoreboardBtn', url: Utils.GetAssetUrl('images/scoreboardbtn.png')});
            assets.push({name: 'questionBtn', url: Utils.GetAssetUrl('images/questionbtn.png')});
            assets.push({name: 'pauseBtn', url: Utils.GetAssetUrl('images/pausebtn.png')});
            assets.push({name: 'resumeBtn', url: Utils.GetAssetUrl('images/resumebtn.png')});
            assets.push({name: 'backBtn', url: Utils.GetAssetUrl('images/backbtn.png')});
            assets.push({name: 'barBackground', url: Utils.GetAssetUrl('images/progress_background.png')});
            assets.push({name: 'barForeground', url: Utils.GetAssetUrl('images/progress_foreground.png')});
            assets.push({name: 'triangleMarker', url: Utils.GetAssetUrl('images/triangle_marker.png')});
            assets.push({name: 'rewardBag', url: Utils.GetAssetUrl('images/icon_reward_bag.png')});
            assets.push({name: 'rewardCase', url: Utils.GetAssetUrl('images/icon_reward_case.png')});
            assets.push({name: 'checkMark', url: Utils.GetAssetUrl('images/checkMark.png')});
            assets.push({name: 'tutHand', url: Utils.GetAssetUrl('images/hand.png')});
            assets.push({name: 'tutSprite', url: Utils.GetAssetUrl('images/tutoverlay.png')});

            //sheet
            bird.meta.image = Utils.GetAssetUrl("images/" + bird.meta.image);
            assets.push({name: "birdAnim", url: "data:@file/json;base64," + Buffer.from(JSON.stringify(bird)).toString("base64")});
            //
            Utils.LoadFont(GameDefine.MAIN_FONT, Utils.GetAssetUrl("fonts/Roboto-Regular.woff"));
            Utils.LoadFont(GameDefine.SUB_FONT_1, Utils.GetAssetUrl("fonts/Bungee-Regular.ttf"));
            // Utils.LoadFont(GameDefine.NUMBER_FONT, Utils.GetAssetUrl("fonts/clearsans-regular.ttf"));
            // Utils.LoadFont(GameDefine.COMBO_FONT, Utils.GetAssetUrl("fonts/clearsans-bold.ttf"));
            Utils.LoadAssets(assets, this.LoadCompleteHandler.bind(this), this.LoadProgressHandler.bind(this), this.LoadErrorhandler.bind(this));
        }
        else
        {
            this.mView.Init();
        }
    }

    Release()
    {
        // console.log('remove');
        // APP.stage.removeChild(this.mView);
        // this.mView.destroy({children:true, texture:true, baseTexture:true});
        // this.SetTouchable(false);
        super.Release();
    }

    Init()
    {
        // PIXI.loader.removeAllListeners();
        // PIXI.loader.reset();
    }

    LoadProgressHandler(loader, res)
    {

    }

    Update(dt)
    {
        // console.log(this.mView);
        this.mView.Update(dt);
    }

    LoadCompleteHandler(loader, res) 
    {
        // this.mView.SetResources(loader.resources);
        // this.mView.Init();
        Resources.birdAnim.frames = (function(){
            let arr = [];
            for (let a in Resources.birdAnim.data.frames)
                arr.push(Resources.birdAnim.textures[a]);
            return arr;
        }());

        // MainMusic.on('load', function(){
        //     // StateManager.SwitchState(GameDefine.GAME_STATE_LOADING, true);
            this.mView.SetResources(loader.resources);
            this.mView.Init();
            StateManager.SwitchState(GameDefine.GAME_STATE_LOADING, true);
        // }.bind(this));
        // MainMusic.onload
        // StateManager.SwitchState(GameDefine.GAME_STATE_LOADING, true);
        // loader.removeAllListeners();
        // this.SetTouchable(true);
    }

    LoadErrorhandler(loader, res) 
    {
        
    }
}

module.exports = StatePreload;