const View = require('../views/BaseView');
const APP = require('../app');

class BaseState
{
    // StateManager = StateManager;
    constructor()
    {
        // super();
        this.mView = new View();
        this.isFadeComplete = false;
        this.mIsFirstInit = false;
    }

    Resume()
    {
        super.Resume();
    }

    Pause()
    {
        super.Pause();
    }

    FadeOut(dt)
    {
        this.mView.alpha = Math.max(0, this.mView.alpha - 2*dt);
        // console.log(this.mView.alpha);
        if (this.mView.alpha == 0)
            this.isFadeComplete = true;
        else
            this.isFadeComplete = false;
    }

    FadeIn(dt)
    {
        this.mView.alpha = Math.min(1, this.mView.alpha + 2*dt);
        if (this.mView.alpha == 1)
            this.isFadeComplete = true;
        else
            this.isFadeComplete = false;
    }

    Update(dt)
    {

    }

    Prelease()
    {

    }

    Release()
    {
        this.mView.removeChildren();
        this.mView.SetTouchable(false);
        APP.stage.removeChild(this.mView);
    }
}

module.exports = BaseState;