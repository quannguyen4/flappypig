class LinkedList
{
    constructor()
    {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    insertAt(data, position = this.length) 
    {
        let node = new Node(data);
        // List is currently empty
        if (this.isEmpty()) 
        {
            return this.insertNew(data);
        }
        // Insertion at head
        if (position == 0) 
        {
            return this.insertAtHead(data);
        }
        let iter = 1;
        let curNode = this.head;
        while (curNode.next != null && iter < position) 
        {
            curNode = curNode.next; 
            iter++;
        }
        // Make new node point to next node in list
        node.next = curNode.next;
        // Make next node's previous point to new node
        if (curNode.next != null) 
        {
            curNode.next.prev = node;
        }
        // Make our node point to previous node
        node.prev = curNode;

        // Make previous node's next point to new node
        curNode.next = node;

        // check if inserted element was at the tail, if yes then make tail point to it
        if (this.tail.next != null) 
        {
            this.tail = this.tail.next;
        }
        this.length++;
        return node;
    }

    removeAt(position = 0) 
    {
        if (this.length === 0) 
        {
            console.log("List is already empty");
            return;
        }
        this.length--;
        let curNode = this.head;
        if (position <= 0) 
        {
            this.head = this.head.next;
            this.head.prev = null;
        } 
        else if (position >= this.length - 1) 
        {
            this.tail = this.tail.prev;
            this.tail.next = null;
        } else 
        {
            let iter = 0;
            while (iter < position) 
            {
                curNode = curNode.next;
                iter++;
            }
            curNode.next = curNode.next.next;
            curNode.next.prev = curNode;
        }
        return curNode;
    }

    getAt(index)
    {
        let iter = 0;
        let node = this.head;
        let curNode = this.head;
        while (iter < index)
        {
            node = curNode.next;
            curNode = curNode.next;
            iter ++;
        }
        return node;
    }

    insertNew(data)
    {
        let node = new Node(data);
        this.head = node;
        this.tail = node;
        this.length++;
        return this.head;
    }

    insertAtHead(data)
    {
        let node = new Node(data);
        if (this.isEmpty())
        {
            this.head = this.insertNew(data);
        }
        else
        {
            node.prev = null;
            node.next = this.head;
            this.head.prev = node;
            this.head = node;
        }
        return this.head;
    }

    insertAtTail(data)
    {
        if (this.tail == null)
        {
            return this.insertAtHead(data);
        }
        else
        {
            let node = new Node(data);
            let tail = this.tail;
            tail.next = node;
            node.prev = tail;
            this.tail = node;
        }
    }

    isEmpty()
    {
        return this.head === null;
    }

    display() 
    {
        let currNode = this.head;
        let arr = [];
        while (currNode != null) {
            arr.push(currNode.data);
            currNode = currNode.next;
        }
        console.log(arr);
    }
}

class Node
{
    constructor(data, next = null, prev = null)
    {
        this.data = data;
        this.next = next;
        this.prev = prev;
    }
}

module.exports = LinkedList;