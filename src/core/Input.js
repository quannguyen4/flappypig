class Input
{
    constructor()
    {
        this.isTouchDown;
        this.isTouchUp;
        this.isTouchMove;
    }

    IsTouchDown(event)
    {
        return event.type == 'mousedown' || event.type == 'touchstart' || event.type == 'pointerdown';
    }

    IsTouchUp(event)
    {
        return event.type == 'mouseup' || event.type == 'touchend' || event.type == 'pointerup';
    }

    IsTouchMove(event)
    {
        return event.type == 'mousemove' || event.type == 'touchmove' || event.type == 'pointermove';
    }
}

module.exports = new Input();