class EventDefine
{
    constructor()
    {
        let n = 0;
        this.ON_CONTAINER_RESIZE                = n++;
        this.STATE_LOADING_COMPLETE             = n++;
        this.REWARD_CONFIG_LOAD_COMPLETE        = n++;
        this.TUTORIAL_VIEW_END                  = n++;
        this.ON_TOUCH                           = n++;
        this.ON_RELEASE                         = n++;
        this.ON_SCORE_SUCCESS                   = n++;
        this.ON_TOUCH_FAIL                      = n++;
        this.ON_WAIT_RESTART                    = n++;
        this.ON_GAME_RESTART                    = n++;
        this.STATE_INGAME_COMPLETE              = n++;
    }
}

module.exports = new EventDefine();