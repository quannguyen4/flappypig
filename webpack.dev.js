const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const { ProvidePlugin } = require('webpack');

const adsTest = 'default';
const inputFilesPath = {
    default: './src/wrapper/default.js',
};

module.exports = merge(common, {
    entry: inputFilesPath[`${adsTest}`],
    mode: 'development',
    devtool: 'inline-source-map',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, `dist/${adsTest}`),
    },
    devServer: {
        contentBase: `./dist/${adsTest}`,
    },
    plugins:[
        new ProvidePlugin({
            Buffer: ['buffer', 'Buffer'],
        }),
    ]
});